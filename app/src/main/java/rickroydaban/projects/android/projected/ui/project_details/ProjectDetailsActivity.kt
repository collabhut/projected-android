package rickroydaban.projects.android.projected.ui.project_details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ProjectDetailsActivityBinding
import rickroydaban.projects.android.projected.ui.project_details.project_components.ProjectComponentsFragment
import rickroydaban.projects.android.projected.ui.project_details.project_tasks.ProjectDetailsTasksFragment
import rickroydaban.projects.android.projected.ui.project_details.project_users.ProjectsUsersFragment

class ProjectDetailsActivity: AppCompatActivity() {

    companion object{
        const val INTENTKEY_PROJECTID = "pid"
        const val INTENTKEY_NAME = "name"
    }

    private lateinit var viewModel: ProjectDetailsViewModel
    private lateinit var adapter: FragmentPagerAdapter
    lateinit var binding: ProjectDetailsActivityBinding
    lateinit var pages: MutableList<ProjectDetailsPage>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProjectDetailsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(ProjectDetailsViewModel::class.java)
        binding.iviewsBack.setOnClickListener { finish() }

        pages = ArrayList()
        pages.apply {
            add(ProjectDetailsTasksFragment(intent.getLongExtra(INTENTKEY_PROJECTID, 0), intent.getStringExtra(INTENTKEY_NAME)?: ""))
            add(ProjectComponentsFragment(intent.getLongExtra(INTENTKEY_PROJECTID, 0)))
            add(ProjectsUsersFragment(intent.getLongExtra(INTENTKEY_PROJECTID, 0)))
        }

        adapter = object : FragmentPagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
            override fun getItem(position: Int): Fragment {
                return pages[position]
            }

            override fun getCount(): Int {
                return pages.size
            }
        }

        binding.tviewsTitle.text = intent.getStringExtra(INTENTKEY_NAME)?: ""
        binding.pager.adapter = adapter
        binding.pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                when(position){
                    0 -> onTasksButtonClick()
                    1 -> onComponentsButtonClick()
                    2 -> onUsersButtonClick()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        binding.tviewsTasks.setOnClickListener {
            binding.pager.currentItem = 0
            onTasksButtonClick()
        }
        binding.tviewsComponents.setOnClickListener {
            binding.pager.currentItem = 1
            onComponentsButtonClick()
        }
        binding.tviewsUsers.setOnClickListener {
            binding.pager.currentItem = 2
            onUsersButtonClick()
        }

        binding.tviewsTasks.post{ binding.tviewsTasks.performClick() }
    }

    private fun onTasksButtonClick(){
        resetAllButtons()
        binding.tviewsTasks.setTextColor(resources.getColor(R.color.blue3))
    }

    private fun onComponentsButtonClick(){
        resetAllButtons()
        binding.tviewsComponents.setTextColor(resources.getColor(R.color.blue3))
    }

    private fun onUsersButtonClick(){
        resetAllButtons()
        binding.tviewsUsers.setTextColor(resources.getColor(R.color.blue3))
    }

    private fun resetAllButtons(){
        binding.tviewsTasks.setTextColor(resources.getColor(R.color.black5))
        binding.tviewsComponents.setTextColor(resources.getColor(R.color.black5))
        binding.tviewsUsers.setTextColor(resources.getColor(R.color.black5))
    }
}