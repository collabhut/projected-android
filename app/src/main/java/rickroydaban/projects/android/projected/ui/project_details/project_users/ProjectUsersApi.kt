package rickroydaban.projects.android.projected.ui.project_details.project_users

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class ProjectUsersApi {
    companion object{
        const val ENDPOINT: String = "project/users.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long): UserRequest()

    class Response(@SerializedName("project_users") val projectUsers: List<ProjectUser>): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class ProjectUser(
        @SerializedName("project_user_id") val projectUserID: Long,
        @SerializedName("fname") val firstName: String,
        @SerializedName("lname") val lastName: String,
        @SerializedName("title") val title: String,
        @SerializedName("pr_edit") val edit: Int,
        @SerializedName("task_add") val addTask: Int
    ): ProjectUsersItem
}