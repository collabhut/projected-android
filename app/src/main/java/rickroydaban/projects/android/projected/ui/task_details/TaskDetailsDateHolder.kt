package rickroydaban.projects.android.projected.ui.task_details

import android.text.SpannableString
import android.text.Spanned
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding
import rickroydaban.projects.android.projected.databinding.TaskDetailsDateRowBinding

class TaskDetailsDateHolder(itemView: View, val binding: TaskDetailsDateRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): TaskDetailsDateHolder {
            val binding = TaskDetailsDateRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsDateHolder(binding.root, binding)
        }
    }

    fun bind(date: TaskDetailsDate){
        val span = SpannableString(date.date)
        span.setSpan(
            UnderlineSpan(),
            0,
            span.toString().length,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.tviewsTitle.text = span
    }
}