package rickroydaban.projects.android.projected.ui.task_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding

class TaskDetailsFooterHolder(itemView: View, val binding: CommonsRowSpace17Binding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): TaskDetailsFooterHolder {
            val binding = CommonsRowSpace17Binding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsFooterHolder(binding.root, binding)
        }
    }
}