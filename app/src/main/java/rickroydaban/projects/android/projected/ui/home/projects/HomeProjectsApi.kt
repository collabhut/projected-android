package rickroydaban.projects.android.projected.ui.home.projects

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse

class HomeProjectsApi {
    companion object{
        const val ENDPOINT: String = "project/list.php"
    }

    class Response(
        @SerializedName("projects") val projects: List<Project>
    ): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class Project(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("name") val name: String
    ): HomeProjectsItem
}