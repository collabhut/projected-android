package rickroydaban.projects.android.projected.ui.task_details

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class TaskDetailsApi {
    companion object{
        const val ENDPOINT: String = "task/details.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long): UserRequest()

    class Response(
        @SerializedName("can_add_subtask") val canAddSubTask: Boolean,
        @SerializedName("can_edit") val canEdit: Boolean,
        @SerializedName("can_see_users") val canSeeUsers: Boolean,
        @SerializedName("can_see_budget") val canSeeBudget: Boolean,
        @SerializedName("can_post") val canPost: Boolean,
        @SerializedName("task_details") val taskDetails: Task,
        @SerializedName("parent_task") val parentTask: Task,
        @SerializedName("subtasks") val subTasks: List<Task>,
        @SerializedName("dates") val dates: List<Date>
    ): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class Task(
        @SerializedName("task_id") val taskID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("target_date") val targetDate: Long,
        @SerializedName("target_date_str") val targetDateStr: String,
        @SerializedName("is_completed") val isCompleted: Int,
        @SerializedName("is_collapsed") val isCollapsed: Int,
        @SerializedName("budget") val budget: Float,
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("project_name") val projectName: String,
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("priority") val priority: Int,
        @SerializedName("task_trail_id") val taskTrailID: Long,
        @SerializedName("type") val type: Int,
        @SerializedName("elapsed") val elapsed: Long,
        @SerializedName("updated_by") val updatedBy: Long,
        @SerializedName("pending") val pending: Int,
        @SerializedName("fname") val fname: String,
        @SerializedName("lname") val lname: String,
        @SerializedName("component") val component: String,
        @SerializedName("lvl") val level: Int,
        @SerializedName("post") val post: String
    ): TaskDetailsItem

    class Date(
        @SerializedName("date") val date: String,
        @SerializedName("trails") val trails: List<Trail>
    )

    class Trail(
        @SerializedName("task_trail_id") val taskTrailID: Long,
        @SerializedName("type") val type: Int,
        @SerializedName("fname") val firstName: String,
        @SerializedName("lname") val lastName: String,
        @SerializedName("user") val user: String,
        @SerializedName("post") val post: String,
        @SerializedName("oldval") val oldVal: String,
        @SerializedName("newval") val newVal: String,
        @SerializedName("time") val time: String
    )
}