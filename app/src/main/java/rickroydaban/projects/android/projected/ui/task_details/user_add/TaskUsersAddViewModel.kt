package rickroydaban.projects.android.projected.ui.task_details.user_add

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class TaskUsersAddViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<TaskUsersAddAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val addUserButtonClickObserver = MutableLiveData<TaskUsersAddListApi.ProjectUser>()
    val onAddUserSuccessObserver = MutableLiveData<Boolean>()

    private var job: Job? = null
    var projectID: Long = 0
    var taskID: Long = 0
    private val header = TaskUsersAddHeader()
    private val footer = TaskUsersAddFooter()
    private var items = ArrayList<TaskUsersAddItem>()
    private var adapter = TaskUsersAddAdapter(items, addUserButtonClickObserver)

    fun init(projectID: Long, taskID: Long){
        this@TaskUsersAddViewModel.projectID = projectID
        this@TaskUsersAddViewModel.taskID = taskID
        adapterSetupObserver.postValue(adapter)
    }

    fun loadUsers(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.usersToAdd(TaskUsersAddListApi.Request(projectID, taskID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.projectUsers)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

//    fun onItemClick(pos: Int){
//        if(items[pos] is HomeTasksApi.MyTask){
//            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
//        }
//    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}