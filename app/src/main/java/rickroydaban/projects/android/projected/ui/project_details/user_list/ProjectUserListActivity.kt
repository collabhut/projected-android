package rickroydaban.projects.android.projected.ui.project_details.user_list

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.databinding.ProjectUserListActivityBinding
import rickroydaban.projects.android.projected.ui.project_details.project_users.ProjectUsersViewModel
import rickroydaban.projects.android.projected.utils.LoaderView

class ProjectUserListActivity: AppCompatActivity() {

    companion object {
        const val INTENTKEY_PROJECTID = "pid"
    }

    private lateinit var binding: ProjectUserListActivityBinding
    lateinit var viewModel: ProjectUsersViewModel
    private lateinit var loader: LoaderView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ProjectUserListActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(ProjectUsersViewModel::class.java)
        loader = LoaderView(binding.loader)
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
//        binding.recyclerview.addOnItemTouchListener(
//            RecyclerItemClickListener(
//                activity, object : RecyclerItemClickListener.OnItemClickListener {
//                    override fun onItemClick(view: View?, position: Int) {
//                        viewModel.onItemClick(position)
//                    }
//                })
//        )

        viewModel.adapterSetupObserver.observe(this, { adapter -> binding.recyclerview.adapter = adapter })
        viewModel.loadObserver.observe(this, { showLoader -> if (showLoader) loader.show() else loader.hide() })
        viewModel.errorObserver.observe(this, { error -> error.process(this) })
//        viewModel.optionsButtonClickObserver.observe(this, { taskUser -> TaskUserListActionBottomSheet(viewModel.projectID, viewModel.taskID, taskUser.taskUserID, "${taskUser.firstName} ${taskUser.lastName}",
//            taskUser.edit>0, taskUser.addSubtask>0, taskUser.seeUsers>0, taskUser.seeBudget>0, taskUser.postUpdate>0).show(supportFragmentManager, TaskUserListActionBottomSheet::class.java.name)})

//        viewModel.navigateToTaskDetailsObserver.observe(viewLifecycleOwner, { myTask ->
//            run {
//                val intent = Intent(activity, TaskDetailsActivity::class.java)
//                intent.putExtra(TaskDetailsActivity.INTENTARG_PROJECTID, myTask.projectID)
//                intent.putExtra(TaskDetailsActivity.INTENTARG_TASKID, myTask.taskID)
//                startActivity(intent)
//            }
//        })

        viewModel.init(intent.getLongExtra(INTENTKEY_PROJECTID, 0))
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadUsers()
        }
//        binding.csAdd.setOnClickListener { run{
//            val intent = Intent(this, TaskUsersAddActivity::class.java)
//            intent.putExtra(TaskUsersAddActivity.INTENTKEY_PROJECTID, viewModel.projectID)
//            intent.putExtra(TaskUsersAddActivity.INTENTKEY_TASKID, viewModel.taskID)
//            startActivity(intent)
//        } }
        binding.iviewsBack.setOnClickListener { finish() }
        viewModel.loadUsers()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadUsers()
    }
}
