package rickroydaban.projects.android.projected.ui.component_create

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ComponentsAddDialogBinding
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsActivity
import rickroydaban.projects.android.projected.utils.FullWidthDialogFragment
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.SimpleTextWatcher

class ComponentCreateDialog(private val projectID: Long): FullWidthDialogFragment() {

    private lateinit var binding: ComponentsAddDialogBinding
    private lateinit var viewModel: ComponentCreateViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ComponentsAddDialogBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(ComponentCreateViewModel::class.java)
        isCancelable = false

        viewModel.init(projectID)
        binding.etextsName.addTextChangedListener(SimpleTextWatcher(object: SimpleTextWatcher.Callback{
            override fun afterTextChanged(s: Editable?) {
                binding.etextsName.setBackgroundResource(if(s!!.isNotEmpty()) R.drawable.bg_roundedrec_border_black3 else R.drawable.bg_roundedrec_border_error)
                binding.labelName.visibility = if(s.isNotEmpty()) View.VISIBLE else View.GONE
            }
        }))

        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.componentCreateSuccessObserver.observe(viewLifecycleOwner) {
            context?.let { context ->
                AlertDialog.Builder(context).setTitle("").setMessage(R.string.success)
                    .setPositiveButton(R.string.ok) { d, _ ->
                        run {
                            if (activity is ProjectDetailsActivity) {
                                (activity as ProjectDetailsActivity).pages[ (activity as ProjectDetailsActivity).binding.pager.currentItem].onResume()
                            }

                            d.dismiss()
                            dismiss()
                        }
                    }.create().show()
            }
        }
        binding.csSubmit.setOnClickListener { viewModel.submit(binding.etextsName.text.toString().trim()) }
        binding.csCancel.setOnClickListener { dismiss() }

        return binding.root
    }
}