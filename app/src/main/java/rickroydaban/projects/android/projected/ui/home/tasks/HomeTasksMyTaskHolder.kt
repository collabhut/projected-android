package rickroydaban.projects.android.projected.ui.home.tasks

import android.app.Activity
import android.content.res.ColorStateList
import android.text.SpannableString
import android.text.Spanned
import android.text.style.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.App
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.HomeTasksTaskRowBinding
import rickroydaban.projects.android.projected.utils.CustomTypefaceSpan

class HomeTasksMyTaskHolder(itemView: View, val binding: HomeTasksTaskRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        const val TYPE_POST = 1
        const val TYPE_CREATE = 2
        const val TYPE_COMPLETE = 3
        const val TYPE_UNCOMPLETE = 4
        const val TYPE_ASSIGNEE_ADD = 5
        const val TYPE_ASSIGNEE_DELETE = 6
        const val TYPE_RENAME = 7
        const val TYPE_REBUDGET = 8
        const val TYPE_REPRIORITY = 9
        const val TYPE_RECOMPONENT = 10
        const val TYPE_RESCHED = 11

        fun new(parent: ViewGroup): HomeTasksMyTaskHolder {
            val binding = HomeTasksTaskRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HomeTasksMyTaskHolder(binding.root, binding)
        }
    }

    fun bind(myTask: HomeTasksApi.MyTask){
        val titleSpan = SpannableString(itemView.context.resources.getString(R.string.task_row_title, myTask.projectName, myTask.component, myTask.taskID.toString(), myTask.name))
        titleSpan.setSpan(
            UnderlineSpan(),
            0,
            titleSpan.toString().indexOf(" #"),
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )

        if(myTask.isCompleted > 0){
            binding.tviewsCount.visibility = View.GONE
            ViewCompat.setBackgroundTintList(
                binding.iviewsCompleted,
                ColorStateList.valueOf(itemView.resources.getColor( R.color.green))
            )

            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black4))
            titleSpan.setSpan(
                ForegroundColorSpan(itemView.resources.getColor(R.color.black4)),
                0,
                titleSpan.toString().indexOf(" #"),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            binding.tviewsLatest.setTextColor(itemView.resources.getColor(R.color.black3))
        }else{
            if(myTask.pending > 0){
                binding.iviewsCompleted.visibility = View.GONE
                binding.tviewsCount.visibility = View.VISIBLE
                binding.tviewsCount.text = myTask.pending.toString()
                if(myTask.priority > 0){
                    binding.tviewsCount.setTextColor(itemView.resources.getColor(R.color.white))
                    ViewCompat.setBackgroundTintList(binding.tviewsCount, ColorStateList.valueOf(itemView.resources.getColor(R.color.red_light)))
                }else{
                    binding.tviewsCount.setTextColor(itemView.resources.getColor(R.color.black))
                    ViewCompat.setBackgroundTintList(binding.tviewsCount, ColorStateList.valueOf(itemView.resources.getColor(R.color.black5)))
                }
            }else{
                binding.iviewsCompleted.visibility = View.VISIBLE
                binding.tviewsCount.visibility = View.GONE
                ViewCompat.setBackgroundTintList(
                    binding.iviewsCompleted,
                    ColorStateList.valueOf(itemView.resources.getColor(if(myTask.priority > 0) R.color.red_light else R.color.black5))
                )
            }

            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black))
            titleSpan.setSpan(
                ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                0,
                titleSpan.toString().indexOf(" #"),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            binding.tviewsLatest.setTextColor(itemView.resources.getColor(R.color.black))
        }

        binding.tviewsTitle.text = titleSpan
        val elapsed = when{
            myTask.elapsed > 60*24*30*12 -> itemView.resources.getString(R.string.x_years_ago, myTask.elapsed/60/24/30/12)
            myTask.elapsed > 60*24*30 -> itemView.resources.getString(R.string.x_months_ago, myTask.elapsed/60/24/30)
            myTask.elapsed > 60*24 -> itemView.resources.getString(R.string.x_days_ago, myTask.elapsed/60/24)
            myTask.elapsed > 60 -> itemView.resources.getString(R.string.x_hours_ago, myTask.elapsed/60)
            myTask.elapsed < 1 -> itemView.resources.getString(R.string.just_now);
            else -> itemView.resources.getString(R.string.x_mins_ago, myTask.elapsed)
        }

        val yes = itemView.resources.getString(R.string.yes)
        val no = itemView.resources.getString(R.string.no)
        val lastActivity = when(myTask.type){
            TYPE_POST -> itemView.resources.getString(R.string.task_row_last_activity_posted, myTask.post)
            TYPE_CREATE -> itemView.resources.getString(R.string.task_row_last_activity_created)
            TYPE_COMPLETE -> itemView.resources.getString(R.string.task_row_last_activity_completed)
            TYPE_UNCOMPLETE -> itemView.resources.getString(R.string.task_row_last_activity_uncompleted)
            TYPE_ASSIGNEE_ADD -> itemView.resources.getString(R.string.task_row_last_activity_add_assignee, myTask.user)
            TYPE_ASSIGNEE_DELETE -> itemView.resources.getString(R.string.task_row_last_activity_remove_assignee, myTask.user)
            TYPE_RENAME -> itemView.context.resources.getString(R.string.task_row_last_activity_rename, myTask.oldVal, myTask.newVal)
            TYPE_REBUDGET -> itemView.context.resources.getString(R.string.task_row_last_activity_rebudget, myTask.oldVal, myTask.newVal)
            TYPE_REPRIORITY -> itemView.context.resources.getString(R.string.task_row_last_activity_reprio, if(myTask.oldVal == "1") yes else no, if(myTask.newVal == "1") yes else no)
            TYPE_RECOMPONENT -> itemView.context.resources.getString(R.string.task_row_last_activity_recomponent, myTask.oldVal, myTask.newVal)
            TYPE_RESCHED -> itemView.context.resources.getString(R.string.task_row_last_activity_resched, myTask.oldVal, myTask.newVal)
            else -> "undefined activity"
        }

        val latestSpan = SpannableString(itemView.context.resources.getString(R.string.task_row_latest_no_budget, myTask.targetDate.uppercase(), myTask.fname, myTask.lname, elapsed, lastActivity))
        latestSpan.setSpan(ForegroundColorSpan(itemView.resources.getColor(if(myTask.isCompleted>0) R.color.black4 else R.color.black)), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        latestSpan.setSpan(CustomTypefaceSpan(((itemView.context as Activity).application as App).customTextViewTypeFace), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        binding.tviewsLatest.text = latestSpan
    }
}