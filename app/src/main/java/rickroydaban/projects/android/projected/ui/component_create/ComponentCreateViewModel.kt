package rickroydaban.projects.android.projected.ui.component_create

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class ComponentCreateViewModel: ViewModel(){

    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val componentCreateSuccessObserver = MutableLiveData<Boolean>()

    private var job: Job? = null
    private var projectID: Long = 0

    fun init(projectID: Long){
        this.projectID = projectID
    }

    fun submit(name: String){
        if(projectID > 0){
            loadObserver.postValue(true)
            job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
                val response = ApiManager.get().projectApi.componentCreate(ComponentCreateApi.Request(projectID, name))
                withContext(Dispatchers.Main){
                    loadObserver.postValue(false)
                    response.body()?.let { body ->
                        if(body.success){
                            componentCreateSuccessObserver.postValue(true)
                        }else{
                            errorObserver.postValue(body.getError())
                        }
                    }
                }
            }
        }else{
            errorObserver.postValue(ApiError(0, "Please fill-up required fields"))
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}