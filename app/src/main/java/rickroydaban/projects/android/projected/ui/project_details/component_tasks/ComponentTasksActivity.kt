//package rickroydaban.projects.android.projected.ui.project_details.component_tasks
//
//import android.content.Intent
//import android.os.Bundle
//import android.view.View
//import androidx.appcompat.app.AppCompatActivity
//import androidx.lifecycle.ViewModelProvider
//import androidx.recyclerview.widget.LinearLayoutManager
//import rickroydaban.projects.android.projected.databinding.ComponentTasksActivityBinding
//import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsActivity
//import rickroydaban.projects.android.projected.utils.LoaderView
//import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener
//
//class ComponentTasksActivity : AppCompatActivity() {
//
//    companion object{
//        const val INTENTKEY_PROJECT_ID = "pid"
//        const val INTENTKEY_COMPONENT_ID = "cid"
//        const val INTENTKEY_COMPONENT_NAME = "cname"
//    }
//
//    private lateinit var binding: ComponentTasksActivityBinding
//    private lateinit var viewModel: ProjectTasksViewModel
//    private lateinit var loader: LoaderView
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        binding = ComponentTasksActivityBinding.inflate(layoutInflater)
//        setContentView(binding.root)
//        viewModel = ViewModelProvider(this).get(ProjectTasksViewModel::class.java)
//        loader = LoaderView(binding.loader)
//        binding.recyclerview.layoutManager = LinearLayoutManager(this)
//        binding.recyclerview.addOnItemTouchListener(
//            RecyclerItemClickListener(
//                this, object : RecyclerItemClickListener.OnItemClickListener {
//                    override fun onItemClick(view: View?, position: Int) {
//                        viewModel.onItemClick(position)
//                    }
//                })
//        )
//        binding.tviewsTitle.text = intent.getStringExtra(INTENTKEY_COMPONENT_NAME)
//        binding.iviewsBack.setOnClickListener { finish() }
//
//        viewModel.adapterSetupObserver.observe(this, {adapter -> binding.recyclerview.adapter = adapter})
//        viewModel.loadObserver.observe(this, {showLoader -> if(showLoader) loader.show() else loader.hide()})
//        viewModel.errorObserver.observe(this, { error -> error.process(this)})
//
//        viewModel.init(intent.getLongExtra(INTENTKEY_PROJECT_ID, 0), intent.getLongExtra(INTENTKEY_COMPONENT_ID, 0))
//        binding.swiperefreshlayout.setOnRefreshListener {
//            binding.swiperefreshlayout.isRefreshing = false
//            viewModel.loadComponentTasks()
//        }
//
//    }
//
//    override fun onResume() {
//        super.onResume()
//        viewModel.loadComponentTasks()
//    }
//
//}