package rickroydaban.projects.android.projected.ui.home.projects

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.data.UserRequest

class HomeProjectsViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<HomeProjectsAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val navigateToProjectDetailsObserver = MutableLiveData<HomeProjectsApi.Project>()

    private var job: Job? = null
    private val header = HomeProjectsHeader()
    private val add = HomeProjectsAdd()
    private val footer = HomeProjectsFooter()
    private var items = ArrayList<HomeProjectsItem>()
    private var adapter = HomeProjectsAdapter(items)

    fun init(){
        adapterSetupObserver.postValue(adapter)
    }

    fun loadMyProjects(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().projectApi.list(UserRequest())
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.projects)
                        items.add(add)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun onItemClick(pos: Int){
        if(items[pos] is HomeProjectsApi.Project){
            navigateToProjectDetailsObserver.postValue(items[pos] as HomeProjectsApi.Project)
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}