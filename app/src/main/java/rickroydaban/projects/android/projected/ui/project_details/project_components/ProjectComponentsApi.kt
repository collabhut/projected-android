package rickroydaban.projects.android.projected.ui.project_details.project_components

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class ProjectComponentsApi {
    companion object{
        const val ENDPOINT: String = "project/components.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long): UserRequest()

    class Response(
        @SerializedName("can_add_task") val canAddTask: Boolean,
        @SerializedName("can_edit") val canEdit: Boolean,
        @SerializedName("name") val name: String,
        @SerializedName("components") val components: List<Component>
    ): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class Component(
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("completed_task_cnt") val completedTaskCount: Int,
        @SerializedName("total_task_cnt") val totalTaskCount: Int
    ): ProjectComponentsItem
}