package rickroydaban.projects.android.projected.ui.task_details.user_add.user_access

import android.app.AlertDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProvider
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskUsersAddSetAccessDialogBinding
import rickroydaban.projects.android.projected.ui.task_details.user_add.TaskUsersAddActivity
import rickroydaban.projects.android.projected.utils.FullWidthDialogFragment
import rickroydaban.projects.android.projected.utils.LoaderView

class TaskUserSetUserAccessDialog(
    val projectID: Long,
    val taskID: Long,
    val projectUserID: Long,
    val name: String
): FullWidthDialogFragment() {

    private lateinit var binding: TaskUsersAddSetAccessDialogBinding
    private lateinit var viewModel: TaskUsersAddUserAccessViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TaskUsersAddSetAccessDialogBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(TaskUsersAddUserAccessViewModel::class.java)
        viewModel.init(projectID, taskID, projectUserID)

        binding.tviewsName.text = name
        binding.csEdit.setOnClickListener {
            if(binding.csEdit.tag.toString().toInt() > 0){
                binding.csEdit.tag = 0
                ViewCompat.setBackgroundTintList(binding.iviewsEdit, ColorStateList.valueOf(resources.getColor( R.color.white5)))
            }else{
                binding.csEdit.tag = 1
                ViewCompat.setBackgroundTintList(binding.iviewsEdit, ColorStateList.valueOf(resources.getColor( R.color.green)))
            }
        }
        binding.csAddSubtask.setOnClickListener {
            if(binding.csAddSubtask.tag.toString().toInt() > 0){
                binding.csAddSubtask.tag = 0
                ViewCompat.setBackgroundTintList(binding.iviewsAddSubtask, ColorStateList.valueOf(resources.getColor( R.color.white5)))
            }else{
                binding.csAddSubtask.tag = 1
                ViewCompat.setBackgroundTintList(binding.iviewsAddSubtask, ColorStateList.valueOf(resources.getColor( R.color.green)))
            }
        }
        binding.csSeeUsers.setOnClickListener {
            if(binding.csSeeUsers.tag.toString().toInt() > 0){
                binding.csSeeUsers.tag = 0
                ViewCompat.setBackgroundTintList(binding.iviewsSeeUsers, ColorStateList.valueOf(resources.getColor( R.color.white5)))
            }else{
                binding.csSeeUsers.tag = 1
                ViewCompat.setBackgroundTintList(binding.iviewsSeeUsers, ColorStateList.valueOf(resources.getColor( R.color.green)))
            }
        }
        binding.csSeeBudget.setOnClickListener {
            if(binding.csSeeBudget.tag.toString().toInt() > 0){
                binding.csSeeBudget.tag = 0
                ViewCompat.setBackgroundTintList(binding.iviewsSeeBudget, ColorStateList.valueOf(resources.getColor( R.color.white5)))
            }else{
                binding.csSeeBudget.tag = 1
                ViewCompat.setBackgroundTintList(binding.iviewsSeeBudget, ColorStateList.valueOf(resources.getColor( R.color.green)))
            }
        }
        binding.csPost.setOnClickListener {
            if(binding.csPost.tag.toString().toInt() > 0){
                binding.csPost.tag = 0
                ViewCompat.setBackgroundTintList(binding.iviewsPostUpdate, ColorStateList.valueOf(resources.getColor( R.color.white5)))
            }else{
                binding.csPost.tag = 1
                ViewCompat.setBackgroundTintList(binding.iviewsPostUpdate, ColorStateList.valueOf(resources.getColor( R.color.green)))
            }
        }
        binding.csSubmit.setOnClickListener {
            val canEdit = binding.csEdit.tag.toString().toInt()
            val canAddSubtask = binding.csAddSubtask.tag.toString().toInt()
            val canSeeUsers = binding.csSeeUsers.tag.toString().toInt()
            val canSeeBudget = binding.csSeeBudget.tag.toString().toInt()
            val canPost = binding.csPost.tag.toString().toInt()
            viewModel.addUser(canEdit, canAddSubtask, canSeeUsers, canSeeBudget, canPost) }
        binding.csCancel.setOnClickListener { dismiss() }

        viewModel.loadObserver.observe(viewLifecycleOwner, {showLoader -> if(showLoader) loader.show() else loader.hide()})
        viewModel.errorObserver.observe(viewLifecycleOwner, { error -> error.process(activity)})
        viewModel.onAddUserSuccessObserver.observe(viewLifecycleOwner, { AlertDialog.Builder(activity).setTitle("").setMessage(R.string.success).setPositiveButton(R.string.ok) { d, _ -> d.dismiss() }.setOnDismissListener {
            if(activity is TaskUsersAddActivity){
                (activity as TaskUsersAddActivity).viewModel.loadUsers()
            }
            dismiss()
        }.create().show()})
        return binding.root
    }
}