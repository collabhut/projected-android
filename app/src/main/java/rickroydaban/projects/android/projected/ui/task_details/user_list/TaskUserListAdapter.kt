package rickroydaban.projects.android.projected.ui.task_details.user_list

import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView

class TaskUserListAdapter(var items: List<TaskUserListItem>, val optionsButtonClickObserver: MutableLiveData<TaskUserListApi.TaskUser>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_TASK_USER: Int = 1
    private val ROW_FOOTER: Int = 2

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is TaskUserListHeader -> ROW_HEADER
            is TaskUserListApi.TaskUser -> ROW_TASK_USER
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> TaskUserListHeaderHolder.new(parent)
            ROW_TASK_USER -> TaskUserListUserHolder.new(parent, optionsButtonClickObserver)
            else -> TaskUserListFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is TaskUserListUserHolder -> {
                val taskUser: TaskUserListApi.TaskUser = items[position] as TaskUserListApi.TaskUser
                holder.bind(taskUser)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}