package rickroydaban.projects.android.projected.ui.task_details.edit

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class EditTaskApi {
    companion object{
        const val ENDPOINT: String = "task/edit.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("task_id") val taskID: Long,
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("target_date") val targetDate: Long,
        @SerializedName("target_date_str") val targetDateStr: String,
        @SerializedName("budget") val budget: Float,
        @SerializedName("priority") val priority: Int): UserRequest()
}