package rickroydaban.projects.android.projected.ui.task_details.create_post

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class TaskDetailsCreatePostApi {
    companion object{
        const val ENDPOINT: String = "task/createpost.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long,
                  @SerializedName("message") val post: String
    ): UserRequest()
}