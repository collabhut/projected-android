package rickroydaban.projects.android.projected.ui.task_details

import android.graphics.Typeface
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class TaskDetailsViewModel: ViewModel(){

    val taskDetailUpdateObserver = MutableLiveData<TaskDetailsApi.Task>()
    val adapterSetupObserver = MutableLiveData<TaskDetailsAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val setTaskDetailCompletedSuccessObserver = MutableLiveData<Boolean>()
    val navigateToTaskDetailsObserver = MutableLiveData<TaskDetailsApi.Task>()
    val editButtonClickObserver = MutableLiveData<Boolean>()
    val addSubTaskButtonClickObserver = MutableLiveData<Boolean>()
    val viewAssigneesButtonClickObserver = MutableLiveData<Boolean>()
    val postUpdateButtonClickObserver = MutableLiveData<Boolean>()

    private var job: Job? = null
    var taskID: Long = 0
    var projectID: Long = 0
    lateinit var taskDetails: TaskDetailsApi.Task
    private lateinit var parentTaskHeader:TaskDetailsHeader
    private lateinit var subTasksHeader:TaskDetailsHeader
    private val footer = TaskDetailsFooter()
    private var items = ArrayList<TaskDetailsItem>()
    private lateinit var adapter: TaskDetailsAdapter

    fun init(projectID: Long, taskID: Long, parentTaskString: String, subTasksString: String, customTypefaceSpan: Typeface){
        this.projectID = projectID
        this.taskID = taskID
        parentTaskHeader = TaskDetailsHeader(parentTaskString)
        subTasksHeader = TaskDetailsHeader(subTasksString)
        adapter = TaskDetailsAdapter(items, editButtonClickObserver, addSubTaskButtonClickObserver, viewAssigneesButtonClickObserver, postUpdateButtonClickObserver, customTypefaceSpan)
        adapterSetupObserver.postValue(adapter)
    }

    fun loadTaskDetails(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.details(TaskDetailsApi.Request(projectID, taskID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        taskDetails = body.taskDetails
                        adapter.setCanSeeUsers(body.canSeeUsers)
                        taskDetailUpdateObserver.postValue(body.taskDetails)
                        items.clear()
                        if(body.parentTask.taskID > 0){
                            items.add(parentTaskHeader)
                            items.add(body.parentTask)
                        }

                        if(body.subTasks.isNotEmpty()){
                            items.add(subTasksHeader)
                            items.addAll(body.subTasks)
                        }

                        items.add(TaskDetailsActions(body.canEdit, body.canAddSubTask, body.canSeeUsers, body.canPost))

                        for(date in body.dates){
                            items.add(TaskDetailsDate(date.date))
                            if(date.trails.isNotEmpty()){
                                val initTrail: TaskDetailsApi.Trail = date.trails[0]
                               items.add(TaskDetailsTrail(initTrail.type, initTrail.time, false, true, initTrail.firstName, initTrail.lastName, initTrail.oldVal, initTrail.newVal, initTrail.post, initTrail.user))
                            }

                            for(i in 1 until date.trails.size){
                                val currTrail: TaskDetailsApi.Trail = date.trails[i]
                                items.add(TaskDetailsTrail(currTrail.type, currTrail.time, true, true, currTrail.firstName, currTrail.lastName, currTrail.oldVal, currTrail.newVal, currTrail.post, currTrail.user))
                            }

                            if(date.trails.isNotEmpty())
                                (items[items.size-1] as TaskDetailsTrail).showBottomLine = false
                        }

                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun setTaskCompleted(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.setTaskCompleted(SetTaskCompletedApi.Request(projectID, taskID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        setTaskDetailCompletedSuccessObserver.postValue(true)
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun onItemClick(pos :Int){
        when(items[pos]){
            is TaskDetailsApi.Task -> navigateToTaskDetailsObserver.postValue(items[pos] as TaskDetailsApi.Task)
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}