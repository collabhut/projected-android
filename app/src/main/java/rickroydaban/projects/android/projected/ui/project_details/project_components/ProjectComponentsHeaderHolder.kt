package rickroydaban.projects.android.projected.ui.project_details.project_components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding

class ProjectComponentsHeaderHolder(itemView: View, val binding: CommonsRowSpace17Binding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): ProjectComponentsHeaderHolder {
            val binding = CommonsRowSpace17Binding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProjectComponentsHeaderHolder(binding.root, binding)
        }
    }
}