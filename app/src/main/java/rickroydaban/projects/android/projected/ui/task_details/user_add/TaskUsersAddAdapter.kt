package rickroydaban.projects.android.projected.ui.task_details.user_add

import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView

class TaskUsersAddAdapter(var items: List<TaskUsersAddItem>, val addUserButtonClickObserver: MutableLiveData<TaskUsersAddListApi.ProjectUser>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_PROJECT_USER: Int = 1
    private val ROW_FOOTER: Int = 2

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is TaskUsersAddHeader -> ROW_HEADER
            is TaskUsersAddListApi.ProjectUser -> ROW_PROJECT_USER
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> TaskUsersAddHeaderHolder.new(parent)
            ROW_PROJECT_USER -> TaskUsersAddUserHolder.new(parent, addUserButtonClickObserver)
            else -> TaskUsersAddFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is TaskUsersAddUserHolder -> {
                val taskUsersAdd: TaskUsersAddListApi.ProjectUser = items[position] as TaskUsersAddListApi.ProjectUser
                holder.bind(taskUsersAdd)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}