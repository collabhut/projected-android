package rickroydaban.projects.android.projected.ui.home.tasks

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.databinding.HomeTasksFragmentBinding
import rickroydaban.projects.android.projected.ui.home.HomePage
import rickroydaban.projects.android.projected.ui.project_details.create_task.CreateTaskDialog
import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsActivity
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener

class HomeTasksFragment : HomePage() {

    private lateinit var binding: HomeTasksFragmentBinding
    private lateinit var viewModel: HomeTasksViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeTasksFragmentBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(HomeTasksViewModel::class.java)
        loader = LoaderView(binding.loader)
        binding.recyclerview.layoutManager = LinearLayoutManager(activity)
        binding.recyclerview.addOnItemTouchListener(
            RecyclerItemClickListener(
                activity, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        viewModel.onItemClick(position)
                    }
                })
        )
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadMyTasks()
        }
        binding.fab.setOnClickListener{ CreateTaskDialog(0, "").show(childFragmentManager, CreateTaskDialog::class.java.name)}
        viewModel.adapterSetupObserver.observe(viewLifecycleOwner) { adapter -> binding.recyclerview.adapter = adapter }
        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(viewLifecycleOwner) { error -> error.process(context) }
        viewModel.navigateToTaskDetailsObserver.observe(viewLifecycleOwner) { myTask ->
            run {
                val intent = Intent(activity, TaskDetailsActivity::class.java)
                intent.putExtra(TaskDetailsActivity.INTENTARG_PROJECTID, myTask.projectID)
                intent.putExtra(TaskDetailsActivity.INTENTARG_TASKID, myTask.taskID)
                startActivity(intent)
            }
        }

        viewModel.init()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        reloadPage()
    }

    override fun reloadPage() {
        viewModel.loadMyTasks()
    }
}