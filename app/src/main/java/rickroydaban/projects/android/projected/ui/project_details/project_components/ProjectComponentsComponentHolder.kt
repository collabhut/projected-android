package rickroydaban.projects.android.projected.ui.project_details.project_components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ProjectComponentsComponentRowBinding

class ProjectComponentsComponentHolder(itemView: View, val binding: ProjectComponentsComponentRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): ProjectComponentsComponentHolder {
            val binding = ProjectComponentsComponentRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProjectComponentsComponentHolder(binding.root, binding)
        }
    }

    fun bind(component: ProjectComponentsApi.Component){
        val progress: Int = ((component.completedTaskCount.toFloat() / component.totalTaskCount) * 100).toInt()
        binding.progressBar.progress = progress
        binding.tviewsProgress.text = "$progress%"
        binding.tviewsTitle.text = component.name
        binding.tviewsSubtitle.text = itemView.resources.getString(R.string.x_done_of_y_tasks, component.completedTaskCount, component.totalTaskCount)
    }
}