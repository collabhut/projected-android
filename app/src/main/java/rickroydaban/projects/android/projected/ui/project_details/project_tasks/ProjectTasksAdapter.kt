package rickroydaban.projects.android.projected.ui.project_details.project_tasks

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ProjectTasksAdapter(var items: List<ProjectTasksItem>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_TASK: Int = 1
    private val ROW_FOOTER: Int = 2

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is ProjectTasksHeader -> ROW_HEADER
            is ProjectTasksApi.ProjectTask -> ROW_TASK
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> ProjectTasksHeaderHolder.new(parent)
            ROW_TASK -> ProjectTasksTaskHolder.new(parent)
            else -> ProjectTasksFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ProjectTasksTaskHolder -> {
                val task: ProjectTasksApi.ProjectTask = items[position] as ProjectTasksApi.ProjectTask
                holder.bind(task)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}