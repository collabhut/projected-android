package rickroydaban.projects.android.projected.ui.task_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding
import rickroydaban.projects.android.projected.databinding.TaskDetailsHeaderRowBinding

class TaskDetailsHeaderHolder(itemView: View, val binding: TaskDetailsHeaderRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): TaskDetailsHeaderHolder {
            val binding = TaskDetailsHeaderRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsHeaderHolder(binding.root, binding)
        }
    }

    fun bind(header: TaskDetailsHeader){
        binding.tviewsTitle.text = header.title
    }
}