package rickroydaban.projects.android.projected.ui.task_details

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskDetailsTrailRowBinding
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder
import rickroydaban.projects.android.projected.utils.CustomTypefaceSpan
import java.lang.Exception

class TaskDetailsTrailHolder(itemView: View, val binding: TaskDetailsTrailRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): TaskDetailsTrailHolder {
            val binding = TaskDetailsTrailRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsTrailHolder(binding.root, binding)
        }
    }

    private val fsizesTitle = itemView.resources.getDimensionPixelSize(R.dimen.fsizes_toolbar)


    fun bind(trail: TaskDetailsTrail, canSeeUsers: Boolean, customTypefaceSpan: Typeface){
        binding.tviewsTime.text = trail.time
        binding.linesTop.visibility = if(trail.showTopLine) View.VISIBLE else View.INVISIBLE
        binding.linesBottom.visibility = if(trail.showBottomLine) View.VISIBLE else View.INVISIBLE

        val yes = itemView.resources.getString(R.string.yes)
        val no = itemView.resources.getString(R.string.no)
        val firstName = if(canSeeUsers) trail.firstName else itemView.resources.getString(R.string.task)
        val lastName = if(canSeeUsers) trail.lastName else itemView.resources.getString(R.string.user)


        val span = SpannableString(when(trail.type){
            HomeTasksMyTaskHolder.TYPE_POST -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_posted, trail.post)
            HomeTasksMyTaskHolder.TYPE_CREATE -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_row_last_activity_created)
            HomeTasksMyTaskHolder.TYPE_COMPLETE -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_row_last_activity_completed)
            HomeTasksMyTaskHolder.TYPE_UNCOMPLETE -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_row_last_activity_uncompleted)
            HomeTasksMyTaskHolder.TYPE_ASSIGNEE_ADD -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_row_last_activity_add_assignee, trail.user)
            HomeTasksMyTaskHolder.TYPE_ASSIGNEE_DELETE -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_row_last_activity_remove_assignee, trail.user)
            HomeTasksMyTaskHolder.TYPE_RENAME -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_rename, trail.oldVal, trail.newVal)
            HomeTasksMyTaskHolder.TYPE_REBUDGET -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_rebudget, trail.oldVal, trail.newVal)
            HomeTasksMyTaskHolder.TYPE_REPRIORITY -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_reprio, if(trail.oldVal == "1") yes else no, if(trail.newVal == "1") yes else no)
            HomeTasksMyTaskHolder.TYPE_RECOMPONENT -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_recomponent, trail.oldVal, trail.newVal)
            HomeTasksMyTaskHolder.TYPE_RESCHED -> firstName+" "+lastName+" "+itemView.resources.getString(R.string.task_details_last_activity_resched, trail.oldVal, trail.newVal)
            else -> "undefined activity"
        })

        span.setSpan(StyleSpan(Typeface.BOLD), 0, firstName.length+lastName.length+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        span.setSpan(ForegroundColorSpan(itemView.resources.getColor(R.color.black3)), 0, firstName.length+lastName.length+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        try {
            if (trail.type == HomeTasksMyTaskHolder.TYPE_POST) {
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().indexOf(trail.post),
                    span.toString().indexOf(trail.post) + trail.post.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
//                span.setSpan(
//                    AbsoluteSizeSpan(fsizesTitle),
//                    span.toString().indexOf(trail.post),
//                    span.toString().indexOf(trail.post) + trail.post.length,
//                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
//                )
            } else if (trail.type == HomeTasksMyTaskHolder.TYPE_RENAME || trail.type == HomeTasksMyTaskHolder.TYPE_REBUDGET || trail.type == HomeTasksMyTaskHolder.TYPE_RECOMPONENT || trail.type == HomeTasksMyTaskHolder.TYPE_RESCHED) {
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().indexOf(trail.oldVal),
                    span.toString().indexOf(trail.oldVal) + trail.oldVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.red_light)),
                    span.toString().indexOf(trail.oldVal),
                    span.toString().indexOf(trail.oldVal) + trail.oldVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().lastIndexOf(trail.newVal),
                    span.toString().lastIndexOf(trail.newVal) + trail.newVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                    span.toString().lastIndexOf(trail.newVal),
                    span.toString().lastIndexOf(trail.newVal) + trail.newVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
            }else if(trail.type == HomeTasksMyTaskHolder.TYPE_REPRIORITY){
                val oldVal = itemView.resources.getString(if(trail.oldVal == "0") R.string.no else R.string.yes)
                val newVal = itemView.resources.getString(if(trail.newVal == "0") R.string.no else R.string.yes)
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().indexOf(oldVal),
                    span.toString().indexOf(oldVal) + oldVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.red_light)),
                    span.toString().indexOf(oldVal),
                    span.toString().indexOf(oldVal) + oldVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().lastIndexOf(newVal),
                    span.toString().lastIndexOf(newVal) + newVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                    span.toString().lastIndexOf(newVal),
                    span.toString().lastIndexOf(newVal) + newVal.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
            }else if (trail.type == HomeTasksMyTaskHolder.TYPE_ASSIGNEE_ADD) {
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().indexOf(trail.user),
                    span.toString().indexOf(trail.user) + trail.user.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                    span.toString().indexOf(trail.user),
                    span.toString().indexOf(trail.user) + trail.user.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
            } else if (trail.type == HomeTasksMyTaskHolder.TYPE_ASSIGNEE_DELETE) {
                span.setSpan(
                    CustomTypefaceSpan(customTypefaceSpan),
                    span.toString().indexOf(trail.user),
                    span.toString().indexOf(trail.user) + trail.user.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
                span.setSpan(
                    ForegroundColorSpan(itemView.resources.getColor(R.color.red_light)),
                    span.toString().indexOf(trail.user),
                    span.toString().indexOf(trail.user) + trail.user.length,
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
        }catch(e: Exception){
            e.printStackTrace()
        }

        binding.tviewsTitle.text = span
    }
}