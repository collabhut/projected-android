package rickroydaban.projects.android.projected.ui.task_details.user_list.actions

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class TaskUserRemoveApi {
    companion object{
        const val ENDPOINT: String = "task/removeuser.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long,
                  @SerializedName("task_user_id") val taskUserID: Long
    ): UserRequest()
}