package rickroydaban.projects.android.projected.ui.project_details.project_users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.databinding.ProjectUsersFragmentBinding
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsPage
import rickroydaban.projects.android.projected.ui.project_details.project_components.ProjectComponentsViewModel
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener

class ProjectsUsersFragment(val projectID: Long): ProjectDetailsPage() {

    private lateinit var binding: ProjectUsersFragmentBinding
    private lateinit var loader: LoaderView
    private lateinit var viewModel: ProjectUsersViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProjectUsersFragmentBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)

        viewModel = ViewModelProvider(this).get(ProjectUsersViewModel::class.java)
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
//        binding.recyclerview.addOnItemTouchListener(
//            RecyclerItemClickListener(
//                activity, object : RecyclerItemClickListener.OnItemClickListener {
//                    override fun onItemClick(view: View?, position: Int) {
//                        viewModel.onItemClick(position)
//                    }
//                })
//        )
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadUsers()
        }
        viewModel.adapterSetupObserver.observe(viewLifecycleOwner) { adapter -> binding.recyclerview.adapter = adapter }
        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(viewLifecycleOwner) { error -> error.process(activity) }

        viewModel.init(projectID)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadUsers()
    }
}