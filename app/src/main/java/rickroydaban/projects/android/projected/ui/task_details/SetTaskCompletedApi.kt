package rickroydaban.projects.android.projected.ui.task_details

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class SetTaskCompletedApi {
    companion object{
        const val ENDPOINT: String = "task/setcompleted.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long): UserRequest()
}