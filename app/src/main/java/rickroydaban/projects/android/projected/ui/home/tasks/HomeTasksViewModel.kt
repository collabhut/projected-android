package rickroydaban.projects.android.projected.ui.home.tasks

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.data.UserRequest

class HomeTasksViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<HomeTasksAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val navigateToTaskDetailsObserver = MutableLiveData<HomeTasksApi.MyTask>()

    private var job: Job? = null
    private val header = HomeTasksHeader()
    private val footer = HomeTasksFooter()
    private var items = ArrayList<HomeTasksItem>()
    private var adapter = HomeTasksAdapter(items)

    fun init(){
        adapterSetupObserver.postValue(adapter)
    }

    fun loadMyTasks(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.myTasks(UserRequest())
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.myTasks)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun onItemClick(pos: Int){
        if(items[pos] is HomeTasksApi.MyTask){
            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}