package rickroydaban.projects.android.projected

import android.app.Application
import android.graphics.Typeface
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.data.SharedPref

class App: Application() {

    lateinit var customTextViewTypeFace: Typeface

    override fun onCreate() {
        super.onCreate()
        customTextViewTypeFace = Typeface.createFromAsset(assets, "fonts/frankleinbold.ttf")
        SharedPref.init(this)
        ApiManager.init(this)
    }


}