package rickroydaban.projects.android.projected.ui.task_details.add_subtask

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class AddSubtaskApi {
    companion object{
        const val ENDPOINT: String = "task/createsubtask.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("parent_task_id") val parentTaskID: Long,
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("target_date") val targetDate: Long,
        @SerializedName("budget") val budget: Float,
        @SerializedName("priority") val priority: Int
    ): UserRequest()
}