package rickroydaban.projects.android.projected.ui.project_details.project_tasks

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ProjectTasksFragmentBinding
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsPage
import rickroydaban.projects.android.projected.ui.project_details.create_task.CreateTaskDialog
import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsActivity
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener

class ProjectDetailsTasksFragment(val projectID: Long, val name: String): ProjectDetailsPage() {

    private lateinit var binding: ProjectTasksFragmentBinding
    private lateinit var loader: LoaderView
    private lateinit var viewModel: ProjectTasksViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProjectTasksFragmentBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)

        viewModel = ViewModelProvider(this).get(ProjectTasksViewModel::class.java)
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.addOnItemTouchListener(
            RecyclerItemClickListener(
                activity, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        viewModel.onItemClick(position)
                    }
                })
        )
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadTasks()
        }
        binding.buttonsComponentsAll.setOnClickListener {
            binding.buttonsComponentsNone.setCardBackgroundColor(resources.getColor(R.color.white))
            (binding.buttonsComponentsNone.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.black2))
            binding.buttonsComponentsAll.setCardBackgroundColor(resources.getColor(R.color.blue3))
            (binding.buttonsComponentsAll.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.white))
            viewModel.setComponent(-1)
        }
        binding.buttonsComponentsNone.setOnClickListener {
            binding.buttonsComponentsAll.setCardBackgroundColor(resources.getColor(R.color.white))
            (binding.buttonsComponentsAll.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.black2))
            binding.buttonsComponentsNone.setCardBackgroundColor(resources.getColor(R.color.blue3))
            (binding.buttonsComponentsNone.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.white))
            viewModel.setComponent(0)
        }
        binding.buttonsAdd.setOnClickListener { CreateTaskDialog(viewModel.projectID, name).show(childFragmentManager, CreateTaskDialog::class.java.name) }
        viewModel.adapterSetupObserver.observe(viewLifecycleOwner) { adapter -> binding.recyclerview.adapter = adapter }
        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(viewLifecycleOwner) { error -> error.process(activity) }
        viewModel.componentsUpdatedObserver.observe(viewLifecycleOwner) { components ->
            binding.csComponents.removeAllViews()
            for(component: ProjectTasksApi.Component in components){
                val v: CardView = LayoutInflater.from(activity).inflate(R.layout.project_tasks_header_row, binding.csComponents, false) as CardView
                v.findViewById<TextView>(R.id.tviews_title).text = component.name
                v.setOnClickListener{
                    viewModel.setComponent(component.componentID)
                }

                if(viewModel.componentID == component.componentID){
                    binding.buttonsComponentsAll.setCardBackgroundColor(resources.getColor(R.color.white))
                    (binding.buttonsComponentsAll.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.black2))
                    binding.buttonsComponentsNone.setCardBackgroundColor(resources.getColor(R.color.white))
                    (binding.buttonsComponentsNone.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.black2))

                    v.setCardBackgroundColor(resources.getColor(R.color.blue3))
                    (v.getChildAt(0) as TextView).setTextColor(resources.getColor(R.color.white))
                }

                binding.csComponents.addView(v)
            }
        }
        viewModel.navigateToTaskDetailsObserver.observe(viewLifecycleOwner) { myTask ->
            run {
                val intent = Intent(activity, TaskDetailsActivity::class.java)
                intent.putExtra(TaskDetailsActivity.INTENTARG_PROJECTID, myTask.projectID)
                intent.putExtra(TaskDetailsActivity.INTENTARG_TASKID, myTask.taskID)
                startActivity(intent)
            }
        }

        viewModel.init(projectID)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadTasks()
    }
}