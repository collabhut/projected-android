package rickroydaban.projects.android.projected.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding

class Row17Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): Row17Holder{
            return Row17Holder(CommonsRowSpace17Binding.inflate(LayoutInflater.from(parent.context), parent, false).root)
        }
    }
}