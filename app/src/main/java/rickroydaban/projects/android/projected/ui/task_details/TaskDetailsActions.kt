package rickroydaban.projects.android.projected.ui.task_details

class TaskDetailsActions(
    val canEdit: Boolean,
    val canAddSubTasks: Boolean,
    val canSeeUsers: Boolean,
    val canPostUpdates: Boolean
): TaskDetailsItem