package rickroydaban.projects.android.projected.ui.home.projects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding
import rickroydaban.projects.android.projected.databinding.HomeProjectsProjectRowBinding

class HomeProjectsProjectHolder(itemView: View, val binding: HomeProjectsProjectRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): HomeProjectsProjectHolder {
            val binding = HomeProjectsProjectRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HomeProjectsProjectHolder(binding.root, binding)
        }
    }

    fun bind(project: HomeProjectsApi.Project){
        binding.tviewsName.text = project.name
    }
}