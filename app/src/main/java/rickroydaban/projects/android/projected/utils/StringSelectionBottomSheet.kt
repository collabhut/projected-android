package rickroydaban.projects.android.projected.utils

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.CommonsStringsBottomsheetBinding
import rickroydaban.projects.android.projected.databinding.CommonsStringsBottomsheetRowBinding

class StringSelectionBottomSheet(private val title: String, private val selections: List<StringSelectionString>, val canAdd: Boolean, val selectObserver: MutableLiveData<StringSelectionString>): BottomSheetDialogFragment() {

    private lateinit var binding: CommonsStringsBottomsheetBinding
    private lateinit var adapter: Adapter
    var items = ArrayList<StringSelectionItem>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.commons_strings_bottomsheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = CommonsStringsBottomsheetBinding.bind(view)

        binding.btAdd.visibility = if(canAdd) View.VISIBLE else View.GONE
        binding.tvTitle.text = title
        items.addAll(selections)
        items.add(StringSelectionSpace())
        binding.recyclerview.layoutManager = LinearLayoutManager(binding.recyclerview.context)
        adapter = Adapter(items)
        binding.recyclerview.adapter = adapter
        binding.recyclerview.addOnItemTouchListener(RecyclerItemClickListener(
            context, object : RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int) {
                    if(items[position] is StringSelectionString){
                        selectObserver.postValue(items[position] as StringSelectionString)
                        dismiss()
                    }
                }
            }))
    }

    class Holder(private val holderBinding: CommonsStringsBottomsheetRowBinding, itemView: View): RecyclerView.ViewHolder(itemView){
        companion object{
            fun new(parent: ViewGroup): Holder {
                val initBinding = CommonsStringsBottomsheetRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return Holder(initBinding, initBinding.root)
            }
        }

        fun bind(stringSelectionString: StringSelectionString){
            holderBinding.tvName.text = stringSelectionString.name
        }
    }

    class Adapter(private val items: List<StringSelectionItem>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun getItemViewType(position: Int): Int {
            return when(items[position]){
                is StringSelectionString -> 1
                else -> 2
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return when(viewType){
                1 -> Holder.new(parent)
                else -> Row17Holder.new(parent)
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if(holder is Holder){
                holder.bind(items[position] as StringSelectionString)
            }
        }

        override fun getItemCount(): Int {
            return items.size
        }

    }

    interface StringSelectionItem{}
    class StringSelectionString(val id: Long, val name: String): StringSelectionItem{}
    class StringSelectionSpace: StringSelectionItem{}
}