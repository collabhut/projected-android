package rickroydaban.projects.android.projected.ui.home.projects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsAddRowBinding

class HomeProjectsAddHolder(itemView: View, val binding: CommonsAddRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): HomeProjectsAddHolder {
            val binding = CommonsAddRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HomeProjectsAddHolder(binding.root, binding)
        }
    }
}