package rickroydaban.projects.android.projected.ui.project_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsAddRowBinding

class ProjectDetailsAddHolder(itemView: View, val binding: CommonsAddRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): ProjectDetailsAddHolder {
            val binding = CommonsAddRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProjectDetailsAddHolder(binding.root, binding)
        }
    }
}