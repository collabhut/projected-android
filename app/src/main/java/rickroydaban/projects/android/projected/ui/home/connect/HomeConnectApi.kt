package rickroydaban.projects.android.projected.ui.home.connect

import rickroydaban.projects.android.projected.data.ApiResponse

class HomeConnectApi {
    companion object{
        const val ENDPOINT: String = "user/connect.php"
    }

    class Response: ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)
}