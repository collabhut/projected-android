package rickroydaban.projects.android.projected.ui.task_details.create_post

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class TaskDetailsCreatePostViewModel: ViewModel(){
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val onPostCreatedObserver = MutableLiveData<Boolean>()

    private var job: Job? = null
    private var projectID: Long = 0
    private var taskID: Long = 0
    private var taskUserID: Long = 0

    fun init(projectID: Long, taskID: Long){
        this@TaskDetailsCreatePostViewModel.projectID = projectID
        this@TaskDetailsCreatePostViewModel.taskID = taskID
    }

    fun createPost(post: String){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.createPost(TaskDetailsCreatePostApi.Request(projectID, taskID, post))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        onPostCreatedObserver.postValue(true)
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

//    fun onItemClick(pos: Int){
//        if(items[pos] is HomeTasksApi.MyTask){
//            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
//        }
//    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}