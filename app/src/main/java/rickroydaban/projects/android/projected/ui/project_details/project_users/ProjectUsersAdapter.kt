package rickroydaban.projects.android.projected.ui.project_details.project_users

import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.ui.project_details.project_users.*

class ProjectUsersAdapter(var items: List<ProjectUsersItem>, val optionsButtonClickObserver: MutableLiveData<ProjectUsersApi.ProjectUser>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_PROJECT_USER: Int = 1
    private val ROW_FOOTER: Int = 2

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is ProjectUsersHeader -> ROW_HEADER
            is ProjectUsersApi.ProjectUser -> ROW_PROJECT_USER
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> ProjectUsersHeaderHolder.new(parent)
            ROW_PROJECT_USER -> ProjectUsersUserHolder.new(parent, optionsButtonClickObserver)
            else -> ProjectUsersFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ProjectUsersUserHolder -> {
                val projectUser: ProjectUsersApi.ProjectUser = items[position] as ProjectUsersApi.ProjectUser
                holder.bind(projectUser)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}