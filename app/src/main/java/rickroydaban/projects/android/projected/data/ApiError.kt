package rickroydaban.projects.android.projected.data

import android.content.Context
import androidx.appcompat.app.AlertDialog
import rickroydaban.projects.android.projected.R

class ApiError(private val code: Int = 1, private var message: String = ""){
    fun process(context: Context?) {
        when(code){
            -1 -> { //invalid access
                showErrorDialog(context!!.resources.getString(R.string.default_error_no_access), context)
            }

            1 -> { //backend issue
                showErrorDialog(context!!.resources.getString(R.string.default_error), context)
                //TODO: Log error to firebase
            }

            2 -> { //duplicate
                if(message == "")
                    message = context!!.resources.getString(R.string.default_error_duplicate)
                showErrorDialog(message, context!!)
            }

            3 -> { //updating self access
                if(message == "")
                    message = context!!.resources.getString(R.string.error_could_not_update_own_access)
                showErrorDialog(message, context!!)
            }

            else -> {
                showErrorDialog(context!!.resources.getString(R.string.default_error_connection), context)
            }
        }
    }

    private fun showErrorDialog(message: String, context: Context){
        AlertDialog.Builder(context)
            .setTitle("")
            .setMessage(message)
            .setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
}
