package rickroydaban.projects.android.projected.data

import com.google.gson.annotations.SerializedName

open class ApiResponse(@SerializedName("success") var success: Boolean, @SerializedName("error") var errorMessage:String, @SerializedName("code") var code: Int) {

    fun getError(): ApiError{
        return ApiError(code, errorMessage)
    }
}