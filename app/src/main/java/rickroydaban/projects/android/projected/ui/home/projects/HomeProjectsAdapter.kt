package rickroydaban.projects.android.projected.ui.home.projects

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class HomeProjectsAdapter(var items: List<HomeProjectsItem>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_PROJECT: Int = 1
    private val ROW_ADD: Int = 2
    private val ROW_FOOTER: Int = 3

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is HomeProjectsHeader -> ROW_HEADER
            is HomeProjectsApi.Project -> ROW_PROJECT
            is HomeProjectsAdd -> ROW_ADD
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> HomeProjectsHeaderHolder.new(parent)
            ROW_PROJECT -> HomeProjectsProjectHolder.new(parent)
            ROW_ADD -> HomeProjectsAddHolder.new(parent)
            else -> HomeProjectsFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is HomeProjectsProjectHolder -> {
                val project: HomeProjectsApi.Project = items[position] as HomeProjectsApi.Project
                holder.bind(project)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}