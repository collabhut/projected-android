package rickroydaban.projects.android.projected.ui.home.tasks

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class HomeTasksAdapter(var items: List<HomeTasksItem>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_TASK: Int = 1
    private val ROW_FOOTER: Int = 2

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is HomeTasksHeader -> ROW_HEADER
            is HomeTasksApi.MyTask -> ROW_TASK
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> HomeTasksHeaderHolder.new(parent)
            ROW_TASK -> HomeTasksMyTaskHolder.new(parent)
            else -> HomeTasksFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is HomeTasksMyTaskHolder -> {
                val task: HomeTasksApi.MyTask = items[position] as HomeTasksApi.MyTask
                holder.bind(task)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}