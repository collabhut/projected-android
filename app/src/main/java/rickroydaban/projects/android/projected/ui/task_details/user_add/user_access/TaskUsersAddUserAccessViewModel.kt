package rickroydaban.projects.android.projected.ui.task_details.user_add.user_access

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class TaskUsersAddUserAccessViewModel: ViewModel(){
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val onAddUserSuccessObserver = MutableLiveData<Boolean>()

    private var job: Job? = null
    private var projectID: Long = 0
    private var taskID: Long = 0
    private var projectUserID: Long = 0

    fun init(projectID: Long, taskID: Long, projectUserID: Long){
        this@TaskUsersAddUserAccessViewModel.projectID = projectID
        this@TaskUsersAddUserAccessViewModel.taskID = taskID
        this@TaskUsersAddUserAccessViewModel.projectUserID = projectUserID
    }

    fun addUser(canEdit: Int, canAddSubtask: Int, canSeeUsers: Int, canSeeUpdates: Int, canPostUpdate: Int){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.addProjectUser(TaskUsersAddAddProjectUserApi.Request(projectID, taskID, projectUserID, canEdit, canAddSubtask, canSeeUsers, canSeeUpdates, canPostUpdate))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        onAddUserSuccessObserver.postValue(true)
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

//    fun onItemClick(pos: Int){
//        if(items[pos] is HomeTasksApi.MyTask){
//            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
//        }
//    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}