package rickroydaban.projects.android.projected.ui.task_details

class TaskDetailsTrail(
    val type: Int,
    val time: String,
    val showTopLine: Boolean,
    var showBottomLine: Boolean,
    val firstName: String,
    val lastName: String,
    val oldVal: String,
    val newVal: String,
    val post: String,
    val user: String
): TaskDetailsItem