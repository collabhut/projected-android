package rickroydaban.projects.android.projected.ui.task_details.create_post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskDetailsPostUpdateDialogBinding
import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsActivity
import rickroydaban.projects.android.projected.utils.FullWidthDialogFragment
import rickroydaban.projects.android.projected.utils.LoaderView

class TaskDetailsCreatePostDialog(val projectID: Long, val taskID: Long): FullWidthDialogFragment() {

    private lateinit var binding: TaskDetailsPostUpdateDialogBinding
    private lateinit var viewModel: TaskDetailsCreatePostViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TaskDetailsPostUpdateDialogBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(TaskDetailsCreatePostViewModel::class.java)
        viewModel.init(projectID, taskID)
        isCancelable = false

        binding.csSubmit.setOnClickListener { viewModel.createPost(binding.etextsPost.text.toString().trim()) }
        binding.csCancel.setOnClickListener { dismiss() }

        viewModel.loadObserver.observe(viewLifecycleOwner, {showLoader -> if(showLoader) loader.show() else loader.hide()})
        viewModel.errorObserver.observe(viewLifecycleOwner, { error -> error.process(activity)})
        viewModel.onPostCreatedObserver.observe(viewLifecycleOwner, { android.app.AlertDialog.Builder(activity).setTitle("").setMessage(R.string.success).setPositiveButton(R.string.ok) { d, _ -> d.dismiss() }.setOnDismissListener { run{
            if(activity is TaskDetailsActivity){
                (activity as TaskDetailsActivity).viewModel.loadTaskDetails()
            }
            dismiss()
        } }.create().show()})

        return binding.root
    }
}