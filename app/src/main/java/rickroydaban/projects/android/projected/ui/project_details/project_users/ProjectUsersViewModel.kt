package rickroydaban.projects.android.projected.ui.project_details.project_users

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class ProjectUsersViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<ProjectUsersAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val optionsButtonClickObserver = MutableLiveData<ProjectUsersApi.ProjectUser>()
    val optionsSelectedObserver = MutableLiveData<StringSelectionBottomSheet.StringSelectionString>()

    var job: Job? = null
    var projectID: Long = 0
    var taskID: Long = 0
    private val header = ProjectUsersHeader()
    private val footer = ProjectUsersFooter()
    private var items = ArrayList<ProjectUsersItem>()
    private var adapter = ProjectUsersAdapter(items, optionsButtonClickObserver)

    fun init(projectID: Long){
        this@ProjectUsersViewModel.projectID = projectID
        adapterSetupObserver.postValue(adapter)
    }

    fun loadUsers(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().projectApi.projectUsers(ProjectUsersApi.Request(projectID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.projectUsers)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

//    fun onItemClick(pos: Int){
//        if(items[pos] is HomeTasksApi.MyTask){
//            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
//        }
//    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}