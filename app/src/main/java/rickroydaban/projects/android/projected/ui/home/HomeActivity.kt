package rickroydaban.projects.android.projected.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import rickroydaban.projects.android.projected.databinding.HomeActivityBinding
import rickroydaban.projects.android.projected.ui.home.projects.HomeProjectsFragment
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksFragment
import rickroydaban.projects.android.projected.ui.home.connect.HomeTeamsFragment
import java.util.ArrayList

class HomeActivity : AppCompatActivity() {
    private lateinit var pages: MutableList<HomePage>
    private lateinit var adapter: FragmentPagerAdapter
    private lateinit var binding: HomeActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = HomeActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pages = ArrayList<HomePage>()
        pages.apply {
            add(HomeTasksFragment())
            add(HomeProjectsFragment())
            add(HomeTeamsFragment())
        }

        adapter = object : FragmentPagerAdapter(
            supportFragmentManager,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        ) {
            override fun getItem(position: Int): Fragment {
                return pages[position]
            }

            override fun getCount(): Int {
                return pages.size
            }
        }

        binding.pager.adapter = adapter
        binding.pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when(position){
                    0 -> onMyTasksButtonClick()
                    1 -> onMyProjectsButtonClick()
                    2 -> onMyTeamsButtonClick()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        binding.buttonsMyTasks.setOnClickListener { onMyTasksButtonClick() }
        binding.buttonsMyProjects.setOnClickListener { onMyProjectsButtonClick() }
        binding.buttonsMyTeams.setOnClickListener { onMyTeamsButtonClick() }
    }

    fun onMyTasksButtonClick() {
        binding.pager.currentItem = 0
        pages[0].reloadPage()
    }

    fun onMyProjectsButtonClick() {
        binding.pager.currentItem = 1
        pages[1].reloadPage()
    }

    fun onMyTeamsButtonClick() {
        binding.pager.currentItem = 2
        pages[2].reloadPage()
    }

    fun loadCurrentPage(){
        pages[binding.pager.currentItem].reloadPage()
    }
}
