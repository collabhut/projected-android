package rickroydaban.projects.android.projected.ui.task_details.user_add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.TaskUsersAddUserRowBinding

class TaskUsersAddUserHolder(itemView: View, val binding: TaskUsersAddUserRowBinding, val addUserButtonClickObserver: MutableLiveData<TaskUsersAddListApi.ProjectUser>) : RecyclerView.ViewHolder(itemView) {

    init {
        binding.csAdd.setOnClickListener {
            addUserButtonClickObserver.postValue(binding.root.tag as TaskUsersAddListApi.ProjectUser)
        }
    }

    companion object{

        fun new(parent: ViewGroup, addUserButtonClickObserver: MutableLiveData<TaskUsersAddListApi.ProjectUser>): TaskUsersAddUserHolder {
            val binding = TaskUsersAddUserRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskUsersAddUserHolder(binding.root, binding, addUserButtonClickObserver)
        }
    }

    fun bind(taskUsersAdd: TaskUsersAddListApi.ProjectUser){
        binding.root.tag = taskUsersAdd
        binding.tviewsTitle.text = "${taskUsersAdd.firstName} ${taskUsersAdd.lastName}"
        binding.tviewsSubtitle.text = taskUsersAdd.title

    }
}