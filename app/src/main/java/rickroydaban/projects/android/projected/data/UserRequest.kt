package rickroydaban.projects.android.projected.data

import com.google.gson.annotations.SerializedName

open class UserRequest {

    @SerializedName("auth") private val auth: String = SharedPref.get().auth

}