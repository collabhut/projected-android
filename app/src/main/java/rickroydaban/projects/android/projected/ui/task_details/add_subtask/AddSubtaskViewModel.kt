package rickroydaban.projects.android.projected.ui.task_details.add_subtask

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.ui.project_details.project_components.ProjectComponentsApi
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet
import java.text.SimpleDateFormat
import java.util.*

class AddSubtaskViewModel: ViewModel(){

    companion object{
        const val MIN_NAME = 5
    }

    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val subTaskCreateSuccessObserver = MutableLiveData<Boolean>()
    val componentListLoadSuccessObserver = MutableLiveData<List<StringSelectionBottomSheet.StringSelectionString>>()
    val selectComponentObserver = MutableLiveData<StringSelectionBottomSheet.StringSelectionString>()
    val taskCreateSuccessObserver = MutableLiveData<Boolean>()
    val selectPriorityObserver = MutableLiveData<StringSelectionBottomSheet.StringSelectionString>()
    val dateSetObserver = MutableLiveData<String>()

    private var job: Job? = null
    private var projectID: Long = 0
    private var taskID: Long = 0
    private var componentID: Long = 0
    private var priority: Boolean = false
    lateinit var datePickerDialog: DatePickerDialog
    private val calendar: Calendar = Calendar.getInstance()
    private val dateFormat: SimpleDateFormat = SimpleDateFormat("MMM dd, yyyy")

    fun init(projectID: Long, taskID: Long,){
        this.projectID = projectID
        this.taskID = taskID
        datePickerDialog = DatePickerDialog.newInstance({ _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.YEAR, year)
            dateSetObserver.postValue(dateFormat.format(calendar.time))
        }, calendar)
        dateSetObserver.postValue(dateFormat.format(calendar.time))
    }

    fun loadComponents(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().projectApi.componentList(ProjectComponentsApi.Request(projectID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        var selections = ArrayList<StringSelectionBottomSheet.StringSelectionString>()

                        for(component: ProjectComponentsApi.Component in body.components){
                            selections.add(StringSelectionBottomSheet.StringSelectionString(component.componentID, component.name))
                        }

                        componentListLoadSuccessObserver.postValue(selections)
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun updateComponent(selection: StringSelectionBottomSheet.StringSelectionString){
        componentID = selection.id
    }

    fun prioritySelectionBottomSheet(yesStr: String, noStr: String, priorityQuestionStr: String): StringSelectionBottomSheet {
        var selections = ArrayList<StringSelectionBottomSheet.StringSelectionString>()
        selections.add(StringSelectionBottomSheet.StringSelectionString(1, yesStr))
        selections.add(StringSelectionBottomSheet.StringSelectionString(0, noStr))
        return StringSelectionBottomSheet(priorityQuestionStr, selections, false, selectPriorityObserver)
    }

    fun updatePriority(selection: StringSelectionBottomSheet.StringSelectionString){
        priority = selection.id == (1.toLong())
    }

    fun createSubTask(taskName: String, budget: Float){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.createSubTask(
                AddSubtaskApi.Request(
                    projectID,
                    taskID,
                    componentID,
                    taskName,
                    calendar.timeInMillis/1000,
                    budget,
                    if(priority) 1 else 0
                )
            )
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        subTaskCreateSuccessObserver.postValue(true)
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}