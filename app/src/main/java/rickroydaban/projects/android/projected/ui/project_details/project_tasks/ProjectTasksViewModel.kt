package rickroydaban.projects.android.projected.ui.project_details.project_tasks

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager

class ProjectTasksViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<ProjectTasksAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val componentsUpdatedObserver = MutableLiveData<List<ProjectTasksApi.Component>>()
    val navigateToTaskDetailsObserver = MutableLiveData<ProjectTasksApi.ProjectTask>()

    private var job: Job? = null
    private val header = ProjectTasksHeader()
    private val footer = ProjectTasksFooter()
    private var items = ArrayList<ProjectTasksItem>()
    private var adapter = ProjectTasksAdapter(items)
    var projectID: Long = 0
    var componentID: Long = -1

    fun init(projectID: Long){
        this@ProjectTasksViewModel .projectID = projectID
        componentID = -1
        adapterSetupObserver.postValue(adapter)
    }

    fun setComponent(componentID: Long){
        this@ProjectTasksViewModel.componentID = componentID
        loadTasks()
    }

    fun loadTasks(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().projectApi.componentTasks(ProjectTasksApi.Request(projectID, componentID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        componentsUpdatedObserver.postValue(body.components)
                        items.clear()
                        items.add(header)
                        items.addAll(body.componentTasks)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun onItemClick(pos: Int){
        if(items[pos] is ProjectTasksApi.ProjectTask){
            navigateToTaskDetailsObserver.postValue(items[pos] as ProjectTasksApi.ProjectTask)
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}