package rickroydaban.projects.android.projected.ui.project_details.project_components

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.ui.project_details.*

class ProjectComponentsAdapter(var componentsItems: List<ProjectComponentsItem>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_COMPONENT: Int = 2
//    private val ROW_ADD: Int = 4
    private val ROW_FOOTER: Int = 6

    override fun getItemViewType(position: Int): Int {
        return when(componentsItems[position]){
            is ProjectComponentsHeader -> ROW_HEADER
            is ProjectComponentsApi.Component -> ROW_COMPONENT
//            is ProjectComponentsAdd -> ROW_ADD
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> ProjectComponentsHeaderHolder.new(parent)
            ROW_COMPONENT -> ProjectComponentsComponentHolder.new(parent)
//            ROW_ADD -> ProjectComponentsAddHolder.new(parent)
            else -> ProjectComponentsFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ProjectComponentsComponentHolder -> {
                val component: ProjectComponentsApi.Component = componentsItems[position] as ProjectComponentsApi.Component
                holder.bind(component)
            }
        }
    }

    override fun getItemCount(): Int {
        return componentsItems.size
    }

}