//package rickroydaban.projects.android.projected.ui.project_details
//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.lifecycle.MutableLiveData
//import androidx.recyclerview.widget.RecyclerView
//import rickroydaban.projects.android.projected.databinding.ProjectDetailsActionsRowBinding
//import rickroydaban.projects.android.projected.databinding.ProjectDetailsComponentRowBinding
//
//class ProjectDetailsActionsHolder(itemView: View, val binding: ProjectDetailsActionsRowBinding,
//                                  private val editButtonClickObserver: MutableLiveData<Boolean>,
//                                  private val addTaskButtonClickObserver: MutableLiveData<Boolean>,
//                                  private val viewUsersButtonClickObserver: MutableLiveData<Boolean>
//) : RecyclerView.ViewHolder(itemView) {
//
//    companion object{
//        fun new(parent: ViewGroup,
//                editButtonClickObserver: MutableLiveData<Boolean>,
//                addTaskButtonClickObserver: MutableLiveData<Boolean>,
//                viewUsersButtonClickObserver: MutableLiveData<Boolean>): ProjectDetailsActionsHolder {
//            val binding = ProjectDetailsActionsRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
//            return ProjectDetailsActionsHolder(binding.root, binding, editButtonClickObserver, addTaskButtonClickObserver, viewUsersButtonClickObserver)
//        }
//    }
//
//    init {
//        binding.buttonsEdit.setOnClickListener { editButtonClickObserver.postValue(true) }
//        binding.buttonsAddSubtask.setOnClickListener { addTaskButtonClickObserver.postValue(true) }
//        binding.buttonsViewAssignees.setOnClickListener { viewUsersButtonClickObserver.postValue(true) }
//    }
//}