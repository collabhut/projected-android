package rickroydaban.projects.android.projected.ui.task_details

import android.app.Activity
import android.content.res.ColorStateList
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StrikethroughSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.App
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskDetailsTaskRowBinding
import rickroydaban.projects.android.projected.utils.CustomTypefaceSpan

class TaskDetailsTaskHolder(itemView: View, val binding: TaskDetailsTaskRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        const val TYPE_POST = 1
        const val TYPE_CREATE = 2
        const val TYPE_COMPLETE = 3
        const val TYPE_UNCOMPLETE = 4
        const val TYPE_ASSIGNEE_ADD = 5
        const val TYPE_ASSIGNEE_DELETE = 6

        fun new(parent: ViewGroup): TaskDetailsTaskHolder {
            val binding = TaskDetailsTaskRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsTaskHolder(binding.root, binding)
        }
    }

    fun bind(task: TaskDetailsApi.Task){
        if(task.isCompleted > 0){
            ViewCompat.setBackgroundTintList(
                binding.iviewsCompleted,
                ColorStateList.valueOf(itemView.resources.getColor( R.color.green))
            )
            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black3))
            val span = SpannableString(itemView.context.resources.getString(R.string.task_row_title, task.projectName, task.component, task.taskID.toString(), task.name))
            span.setSpan(StrikethroughSpan(), 0, span.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
            binding.tviewsTitle.text = span
        }else{
            if(task.pending > 0){
                binding.iviewsCompleted.visibility = View.GONE
                binding.tviewsCount.visibility = View.VISIBLE
                binding.tviewsCount.text = task.pending.toString()
            }else{
                binding.iviewsCompleted.visibility = View.VISIBLE
                binding.tviewsCount.visibility = View.GONE
                ViewCompat.setBackgroundTintList(
                    binding.iviewsCompleted,
                    ColorStateList.valueOf(itemView.resources.getColor( R.color.white5))
                )
            }

            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black))
            val span = SpannableString(itemView.context.resources.getString(R.string.task_row_title, task.projectName, task.component, task.taskID.toString(), task.name))
            span.setSpan(
                ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                0,
                span.toString().indexOf(" #" + task.taskID.toString()),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            span.setSpan(
                UnderlineSpan(),
                0,
                span.toString().indexOf(" #" + task.taskID.toString()),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            binding.tviewsTitle.text = span
        }


        val elapsed = when{
            task.elapsed > 60*24*30*12 -> itemView.resources.getString(R.string.x_years_ago, task.elapsed/60/24/30/12)
            task.elapsed > 60*24*30 -> itemView.resources.getString(R.string.x_months_ago, task.elapsed/60/24/30)
            task.elapsed > 60*24 -> itemView.resources.getString(R.string.x_days_ago, task.elapsed/60/24)
            task.elapsed > 60 -> itemView.resources.getString(R.string.x_hours_ago, task.elapsed/60)
            else -> itemView.resources.getString(R.string.x_mins_ago, task.elapsed)
        }

        val lastActivity = when(task.type){
            TYPE_POST -> itemView.context.resources.getString(R.string.task_details_last_activity_posted, task.post)
            TYPE_CREATE -> itemView.context.resources.getString(R.string.task_row_last_activity_created)
            TYPE_COMPLETE -> itemView.context.resources.getString(R.string.task_row_last_activity_completed)
            TYPE_UNCOMPLETE -> itemView.context.resources.getString(R.string.task_row_last_activity_uncompleted)
            TYPE_ASSIGNEE_ADD -> itemView.context.resources.getString(R.string.task_row_last_activity_add_assignee)
            TYPE_ASSIGNEE_DELETE -> itemView.context.resources.getString(R.string.task_row_last_activity_remove_assignee)
            else -> "undefined activity"
        }


        val latestSpan = SpannableString(itemView.context.resources.getString(R.string.task_row_latest_no_budget, task.targetDateStr.uppercase(), task.fname, task.lname, elapsed, lastActivity))
        latestSpan.setSpan(ForegroundColorSpan(itemView.resources.getColor(if(task.isCompleted>0) R.color.black4 else R.color.black)), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        latestSpan.setSpan(CustomTypefaceSpan(((itemView.context as Activity).application as App).customTextViewTypeFace), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        binding.tviewsLatest.text = latestSpan
    }
}