package rickroydaban.projects.android.projected.ui.project_details.create_task

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class CreateTaskApi {
    companion object{
        const val ENDPOINT: String = "project/createtask.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("target_date") val targetDate: Long,
        @SerializedName("budget") val budget: Float,
        @SerializedName("priority") val priority: Int
    ): UserRequest()
}