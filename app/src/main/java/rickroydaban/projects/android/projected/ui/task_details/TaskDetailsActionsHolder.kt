package rickroydaban.projects.android.projected.ui.task_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding
import rickroydaban.projects.android.projected.databinding.TaskDetailsActionsRowBinding

class TaskDetailsActionsHolder(itemView: View,
                               val binding: TaskDetailsActionsRowBinding,
                               private val editButtonClickObserver: MutableLiveData<Boolean>,
                               private val addSubTaskButtonClickObserver: MutableLiveData<Boolean>,
                               private val viewAssigneesButtonClickObserver: MutableLiveData<Boolean>,
                               private val postUpdateButtonClickObserver: MutableLiveData<Boolean>) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup,
                editButtonClickObserver: MutableLiveData<Boolean>,
                addSubTaskButtonClickObserver: MutableLiveData<Boolean>,
                viewAssigneesButtonClickObserver: MutableLiveData<Boolean>,
                postUpdateButtonClickObserver: MutableLiveData<Boolean>): TaskDetailsActionsHolder {
            val binding = TaskDetailsActionsRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskDetailsActionsHolder(binding.root, binding, editButtonClickObserver, addSubTaskButtonClickObserver, viewAssigneesButtonClickObserver, postUpdateButtonClickObserver)
        }
    }

    fun bind(taskDetailsActions: TaskDetailsActions){
        binding.buttonsEdit.visibility = if(taskDetailsActions.canEdit) View.VISIBLE else View.GONE
        binding.buttonsAddSubtask.visibility = if(taskDetailsActions.canAddSubTasks) View.VISIBLE else View.GONE
        binding.buttonsViewAssignees.visibility = if(taskDetailsActions.canSeeUsers) View.VISIBLE else View.GONE
        binding.buttonsPost.visibility = if(taskDetailsActions.canPostUpdates) View.VISIBLE else View.GONE
        binding.buttonsEdit.setOnClickListener { editButtonClickObserver.postValue(true) }
        binding.buttonsAddSubtask.setOnClickListener { addSubTaskButtonClickObserver.postValue(true) }
        binding.buttonsViewAssignees.setOnClickListener { viewAssigneesButtonClickObserver.postValue(true) }
        binding.buttonsPost.setOnClickListener { postUpdateButtonClickObserver.postValue(true) }
    }
}