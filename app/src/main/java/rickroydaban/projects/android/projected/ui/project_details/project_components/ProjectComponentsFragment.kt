package rickroydaban.projects.android.projected.ui.project_details.project_components

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ProjectComponentsFragmentBinding
import rickroydaban.projects.android.projected.ui.component_create.ComponentCreateDialog
import rickroydaban.projects.android.projected.ui.component_rename.ComponentRenameDialog
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsActivity
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsPage
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class ProjectComponentsFragment(val projectID: Long): ProjectDetailsPage() {

    private lateinit var binding: ProjectComponentsFragmentBinding
    private lateinit var loader: LoaderView
    private lateinit var viewModel: ProjectComponentsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProjectComponentsFragmentBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)

        viewModel = ViewModelProvider(this).get(ProjectComponentsViewModel::class.java)
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.addOnItemTouchListener(
            RecyclerItemClickListener(
                activity, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        viewModel.onItemClick(position)
                    }
                })
        )
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadComponents()
        }
        binding.buttonsAdd.setOnClickListener { ComponentCreateDialog(projectID).show(childFragmentManager, ComponentCreateDialog::class.java.name) }

        viewModel.adapterSetupObserver.observe(viewLifecycleOwner) { adapter -> binding.recyclerview.adapter = adapter }
        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(viewLifecycleOwner) { error -> error.process(activity) }
        viewModel.componentItemSelectedObserver.observe(viewLifecycleOwner){ component ->
            binding.recyclerview.tag = component
            viewModel.getOptionsBottomSheet(resources.getString(R.string.rename), resources.getString(R.string.delete)).show(childFragmentManager, StringSelectionBottomSheet::class.java.name)
        }
        viewModel.optionSelectedObserver.observe(viewLifecycleOwner){ selection ->
            val component: ProjectComponentsApi.Component = binding.recyclerview.tag as ProjectComponentsApi.Component
            if(selection.id.toInt() == 1){
                ComponentRenameDialog(viewModel.projectID, component.componentID, component.name).show(childFragmentManager, ComponentRenameDialog::class.java.name)
            }else{
                AlertDialog.Builder(requireContext()).setTitle("").setMessage(resources.getString(R.string.confirm_delete_component, component.name)).setNegativeButton(R.string.no){ d,_ ->
                    viewModel.getOptionsBottomSheet(resources.getString(R.string.rename), resources.getString(R.string.delete)).show(childFragmentManager, StringSelectionBottomSheet::class.java.name)
                    d.dismiss()
                }.setPositiveButton(R.string.yes){ d,_ ->
                    viewModel.deleteComponent(component.componentID)
                    d.dismiss() }.create().show()
            }
        }
        viewModel.componentDeleteSuccessObserver.observe(viewLifecycleOwner) {
            context?.let { context ->
                AlertDialog.Builder(context).setTitle("").setMessage(R.string.success)
                    .setPositiveButton(R.string.ok) { d, _ ->
                        run {
                            viewModel.loadComponents()
                            d.dismiss()
                        }
                    }.create().show()
            }
        }

        viewModel.init(projectID)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadComponents()
    }
}