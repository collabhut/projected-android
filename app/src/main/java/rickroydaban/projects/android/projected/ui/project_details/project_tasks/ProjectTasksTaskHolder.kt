package rickroydaban.projects.android.projected.ui.project_details.project_tasks

import android.app.Activity
import android.content.res.ColorStateList
import android.text.SpannableString
import android.text.Spanned
import android.text.style.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.App
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.HomeTasksTaskRowBinding
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_ASSIGNEE_ADD
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_ASSIGNEE_DELETE
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_COMPLETE
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_CREATE
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_POST
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_REBUDGET
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_RECOMPONENT
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_RENAME
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_REPRIORITY
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_RESCHED
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksMyTaskHolder.Companion.TYPE_UNCOMPLETE
import rickroydaban.projects.android.projected.utils.CustomTypefaceSpan

class ProjectTasksTaskHolder(itemView: View, val binding: HomeTasksTaskRowBinding) : RecyclerView.ViewHolder(itemView) {

    companion object{

        fun new(parent: ViewGroup): ProjectTasksTaskHolder {
            val binding = HomeTasksTaskRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProjectTasksTaskHolder(binding.root, binding)
        }
    }

    fun bind(projectTask: ProjectTasksApi.ProjectTask){
        val titleSpan = SpannableString(itemView.context.resources.getString(R.string.task_row_title, projectTask.projectName, projectTask.component, projectTask.taskID.toString(), projectTask.name))
        titleSpan.setSpan(
            UnderlineSpan(),
            0,
            titleSpan.toString().indexOf(" #"),
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )

        if(projectTask.isCompleted > 0){
            ViewCompat.setBackgroundTintList(
                binding.iviewsCompleted,
                ColorStateList.valueOf(itemView.resources.getColor( R.color.green))
            )

            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black4))
            titleSpan.setSpan(
                ForegroundColorSpan(itemView.resources.getColor(R.color.black4)),
                0,
                titleSpan.toString().indexOf(" #"),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            binding.tviewsLatest.setTextColor(itemView.resources.getColor(R.color.black3))
        }else{
            if(projectTask.pending > 0){
                binding.iviewsCompleted.visibility = View.GONE
                binding.tviewsCount.visibility = View.VISIBLE
                binding.tviewsCount.text = projectTask.pending.toString()
            }else{
                binding.iviewsCompleted.visibility = View.VISIBLE
                binding.tviewsCount.visibility = View.GONE
                ViewCompat.setBackgroundTintList(
                    binding.iviewsCompleted,
                    ColorStateList.valueOf(itemView.resources.getColor(if(projectTask.priority > 0) R.color.red_light else R.color.black5))
                )
            }

            binding.tviewsTitle.setTextColor(itemView.resources.getColor(R.color.black))
            titleSpan.setSpan(
                ForegroundColorSpan(itemView.resources.getColor(R.color.blue3)),
                0,
                titleSpan.toString().indexOf(" #"),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
            )
            binding.tviewsLatest.setTextColor(itemView.resources.getColor(R.color.black))
        }

        binding.tviewsTitle.text = titleSpan
        val elapsed = when{
            projectTask.elapsed > 60*24*30*12 -> itemView.resources.getString(R.string.x_years_ago, projectTask.elapsed/60/24/30/12)
            projectTask.elapsed > 60*24*30 -> itemView.resources.getString(R.string.x_months_ago, projectTask.elapsed/60/24/30)
            projectTask.elapsed > 60*24 -> itemView.resources.getString(R.string.x_days_ago, projectTask.elapsed/60/24)
            projectTask.elapsed > 60 -> itemView.resources.getString(R.string.x_hours_ago, projectTask.elapsed/60)
            projectTask.elapsed < 1 -> itemView.resources.getString(R.string.just_now);
            else -> itemView.resources.getString(R.string.x_mins_ago, projectTask.elapsed)
        }

        val yes = itemView.resources.getString(R.string.yes)
        val no = itemView.resources.getString(R.string.no)
        val lastActivity = when(projectTask.type){
            TYPE_POST -> itemView.resources.getString(R.string.task_row_last_activity_posted, projectTask.post)
            TYPE_CREATE -> itemView.resources.getString(R.string.task_row_last_activity_created)
            TYPE_COMPLETE -> itemView.resources.getString(R.string.task_row_last_activity_completed)
            TYPE_UNCOMPLETE -> itemView.resources.getString(R.string.task_row_last_activity_uncompleted)
            TYPE_ASSIGNEE_ADD -> itemView.resources.getString(R.string.task_row_last_activity_add_assignee, projectTask.user)
            TYPE_ASSIGNEE_DELETE -> itemView.resources.getString(R.string.task_row_last_activity_remove_assignee, projectTask.user)
            TYPE_RENAME -> itemView.context.resources.getString(R.string.task_row_last_activity_rename, projectTask.oldVal, projectTask.newVal)
            TYPE_REBUDGET -> itemView.context.resources.getString(R.string.task_row_last_activity_rebudget, projectTask.oldVal, projectTask.newVal)
            TYPE_REPRIORITY -> itemView.context.resources.getString(R.string.task_row_last_activity_reprio, if(projectTask.oldVal == "1") yes else no, if(projectTask.newVal == "1") yes else no)
            TYPE_RECOMPONENT -> itemView.context.resources.getString(R.string.task_row_last_activity_recomponent, projectTask.oldVal, projectTask.newVal)
            TYPE_RESCHED -> itemView.context.resources.getString(R.string.task_row_last_activity_resched, projectTask.oldVal, projectTask.newVal)
            else -> "undefined activity"
        }

        val latestSpan = SpannableString(itemView.context.resources.getString(R.string.task_row_latest_no_budget, projectTask.targetDate.uppercase(), projectTask.fname, projectTask.lname, elapsed, lastActivity))
        latestSpan.setSpan(ForegroundColorSpan(itemView.resources.getColor(if(projectTask.isCompleted>0) R.color.black4 else R.color.black)), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        latestSpan.setSpan(CustomTypefaceSpan(((itemView.context as Activity).application as App).customTextViewTypeFace), 2, 14, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        binding.tviewsLatest.text = latestSpan
    }
}