package rickroydaban.projects.android.projected.ui.project_details.project_components

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class ComponentDeleteApi {
    companion object{
        const val ENDPOINT: String = "project/deletecomponent.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("component_id") val componentID: Long
    ): UserRequest()
}