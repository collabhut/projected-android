package rickroydaban.projects.android.projected.ui.project_details.project_tasks

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class ProjectTasksApi {
    companion object{
        const val ENDPOINT: String = "project/tasks.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("component_id") val componentID: Long): UserRequest()

    class Response(
        @SerializedName("components") val components: List<Component>,
        @SerializedName("component_tasks") val componentTasks: List<ProjectTask>
    ): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class Component(
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String
    )

    class ProjectTask(
        @SerializedName("task_id") val taskID: Long,
        @SerializedName("name") val name: String,
        @SerializedName("target_date") val targetDate: String,
        @SerializedName("is_completed") val isCompleted: Int,
        @SerializedName("is_collapsed") val isCollapsed: Int,
        @SerializedName("priority") val priority: Int,
        @SerializedName("budget") val budget: Float,
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("project_name") val projectName: String,
        @SerializedName("task_trail_id") val taskTrailID: Long,
        @SerializedName("type") val type: Int,
        @SerializedName("elapsed") val elapsed: Long,
        @SerializedName("updated_by") val updatedBy: Long,
        @SerializedName("pending") val pending: Int,
        @SerializedName("fname") val fname: String,
        @SerializedName("lname") val lname: String,
        @SerializedName("component") val component: String,
        @SerializedName("post") val post: String,
        @SerializedName("oldval") val oldVal: String,
        @SerializedName("newval") val newVal: String,
        @SerializedName("user") val user: String
    ): ProjectTasksItem
}