package rickroydaban.projects.android.projected.data

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import rickroydaban.projects.android.projected.ui.component_create.ComponentCreateApi
import rickroydaban.projects.android.projected.ui.home.projects.HomeProjectsApi
import rickroydaban.projects.android.projected.ui.project_details.project_components.ComponentDeleteApi
import rickroydaban.projects.android.projected.ui.component_rename.ComponentRenameApi
import rickroydaban.projects.android.projected.ui.project_details.project_components.ProjectComponentsApi
import rickroydaban.projects.android.projected.ui.project_details.project_tasks.ProjectTasksApi
import rickroydaban.projects.android.projected.ui.project_details.create_task.CreateTaskApi
import rickroydaban.projects.android.projected.ui.project_details.project_users.ProjectUsersApi

interface ProjectApi {
    @POST(HomeProjectsApi.ENDPOINT)
    suspend fun list(@Body body: UserRequest?): Response<HomeProjectsApi.Response>

    @POST(CreateTaskApi.ENDPOINT)
    suspend fun createTask(@Body body: CreateTaskApi.Request): Response<ApiResponse>

    @POST(ProjectTasksApi.ENDPOINT)
    suspend fun componentTasks(@Body body: ProjectTasksApi.Request): Response<ProjectTasksApi.Response?>

    @POST(ProjectUsersApi.ENDPOINT)
    suspend fun projectUsers(@Body body: ProjectUsersApi.Request): Response<ProjectUsersApi.Response?>

    @POST(ProjectComponentsApi.ENDPOINT)
    suspend fun componentList(@Body body: ProjectComponentsApi.Request): Response<ProjectComponentsApi.Response>

    @POST(ComponentCreateApi.ENDPOINT)
    suspend fun componentCreate(@Body body: ComponentCreateApi.Request): Response<ApiResponse>

    @POST(ComponentRenameApi.ENDPOINT)
    suspend fun componentRename(@Body body: ComponentRenameApi.Request): Response<ApiResponse>

    @POST(ComponentDeleteApi.ENDPOINT)
    suspend fun componentDelete(@Body body: ComponentDeleteApi.Request): Response<ApiResponse>
}