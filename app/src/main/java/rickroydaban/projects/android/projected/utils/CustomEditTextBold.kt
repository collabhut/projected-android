package rickroydaban.projects.android.projected.utils

import android.content.Context
import androidx.appcompat.widget.AppCompatEditText
import android.graphics.Typeface
import android.util.AttributeSet

class CustomEditTextBold : AppCompatEditText {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(
        context, attrs
    ) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context, attrs, defStyleAttr
    ) {
        init()
    }

    private fun init() {
        typeface = Typeface.createFromAsset(context.assets, "fonts/frankleinbold.ttf")
    }
}