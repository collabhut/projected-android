package rickroydaban.projects.android.projected.ui.task_details.user_list.edit_user_access

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class TaskUserEditAccessApi {
    companion object{
        const val ENDPOINT: String = "task/edittaskuseraccess.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long,
                  @SerializedName("task_user_id") val taskUserID: Long,
                  @SerializedName("can_edit") val canEdit: Int,
                  @SerializedName("can_add_subtask") val canAddSubtask: Int,
                  @SerializedName("can_see_users") val canSeeUsers: Int,
                  @SerializedName("can_see_budget") val canSeeBudget: Int,
                  @SerializedName("can_post") val canPost: Int
    ): UserRequest()
}