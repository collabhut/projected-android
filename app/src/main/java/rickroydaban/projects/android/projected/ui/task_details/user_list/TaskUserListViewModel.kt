package rickroydaban.projects.android.projected.ui.task_details.user_list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class TaskUserListViewModel: ViewModel(){
    val adapterSetupObserver = MutableLiveData<TaskUserListAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val optionsButtonClickObserver = MutableLiveData<TaskUserListApi.TaskUser>()
    val optionsSelectedObserver = MutableLiveData<StringSelectionBottomSheet.StringSelectionString>()

    var job: Job? = null
    var projectID: Long = 0
    var taskID: Long = 0
    private val header = TaskUserListHeader()
    private val footer = TaskUserListFooter()
    private var items = ArrayList<TaskUserListItem>()
    private var adapter = TaskUserListAdapter(items, optionsButtonClickObserver)

    fun init(projectID: Long, taskID: Long){
        this@TaskUserListViewModel.projectID = projectID
        this@TaskUserListViewModel.taskID = taskID
        adapterSetupObserver.postValue(adapter)
    }

    fun loadUsers(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().taskApi.usersToAdd(TaskUserListApi.Request(projectID, taskID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.taskUsers)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

//    fun onItemClick(pos: Int){
//        if(items[pos] is HomeTasksApi.MyTask){
//            navigateToTaskDetailsObserver.postValue(items[pos] as HomeTasksApi.MyTask)
//        }
//    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}