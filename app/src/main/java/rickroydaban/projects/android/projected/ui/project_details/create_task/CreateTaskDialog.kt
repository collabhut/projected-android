package rickroydaban.projects.android.projected.ui.project_details.create_task

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskCreateDialogBinding
import rickroydaban.projects.android.projected.ui.home.HomeActivity
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsActivity
import rickroydaban.projects.android.projected.ui.task_details.edit.EditTaskViewModel
import rickroydaban.projects.android.projected.utils.FullWidthDialogFragment
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.SimpleTextWatcher
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class CreateTaskDialog(val projectID: Long, val projectName: String): FullWidthDialogFragment() {
    private lateinit var binding: TaskCreateDialogBinding
    private lateinit var viewModel: CreateTaskViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TaskCreateDialogBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(CreateTaskViewModel::class.java)
        viewModel.init(projectID)
        if(projectID > 0) {
            binding.tviewsName.text = resources.getString(R.string.create_new_task_for_project, projectName)
            binding.csProject.visibility = View.GONE
        }
        else  {
            binding.tviewsName.text = resources.getString(R.string.create_task)
            binding.csProject.visibility = View.VISIBLE
            binding.tviewsProject.setOnClickListener { viewModel.loadProjects() }

            viewModel.projectListLoadSuccessObserver.observe(viewLifecycleOwner) { items ->
                run {
                    StringSelectionBottomSheet(
                        resources.getString(R.string.select_project),
                        items,
                        false,
                        viewModel.selectProjectObserver
                    ).show(childFragmentManager, StringSelectionBottomSheet::class.java.name)
                }
            }
            viewModel.selectProjectObserver.observe(viewLifecycleOwner) { stringSelection ->
                run {
                    binding.tviewsProject.text = stringSelection.name
                    viewModel.updateProject(stringSelection)
                    binding.labelProject.visibility =  View.VISIBLE
                    binding.tviewsProject.setBackgroundResource(R.drawable.bg_roundedrec_border_black3)
                }
            }
        }

        binding.tviewsComponent.setOnClickListener { viewModel.loadComponents() }
        binding.etextsName.addTextChangedListener(SimpleTextWatcher(object: SimpleTextWatcher.Callback{
            override fun afterTextChanged(s: Editable?) {
                binding.labelName.visibility = if(s!!.isNotEmpty()) View.VISIBLE else View.GONE
                binding.etextsName.setBackgroundResource(if(s.length < EditTaskViewModel.MIN_NAME) R.drawable.bg_roundedrec_border_error else R.drawable.bg_roundedrec_border_black3)
            }
        }))
        binding.tviewsTargetDate.setOnClickListener { viewModel.datePickerDialog.show(childFragmentManager, DatePickerDialog::class.java.name) }
        binding.tviewsPriority.setOnClickListener { viewModel.prioritySelectionBottomSheet(
            resources.getString(R.string.yes),
            resources.getString(R.string.no),
            resources.getString(R.string.priority_question)
        ).show(childFragmentManager, StringSelectionBottomSheet::class.java.name) }

        viewModel.loadObserver.observe(this) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(this) { error -> error.process(activity) }
        viewModel.componentListLoadSuccessObserver.observe(viewLifecycleOwner) { items ->
            run {
                StringSelectionBottomSheet(
                    resources.getString(R.string.select_component),
                    items,
                    false,
                    viewModel.selectComponentObserver
                ).show(childFragmentManager, StringSelectionBottomSheet::class.java.name)
            }
        }
        viewModel.selectComponentObserver.observe(viewLifecycleOwner) { stringSelection ->
            run {
                binding.tviewsComponent.text = stringSelection.name
                viewModel.updateComponent(stringSelection)
            }
        }
        viewModel.taskCreateSuccessObserver.observe(viewLifecycleOwner) {
            run {
                AlertDialog.Builder(requireContext()).setTitle("").setMessage(R.string.success)
                    .setPositiveButton(R.string.ok) { d, _ ->
                        run {
//                            if (activity is ProjectDetailsActivity) {
//                                (activity as ProjectDetailsActivity).viewModel.loadProjectDetails()
//                            }

                            if(activity is HomeActivity){
                                (activity as HomeActivity).loadCurrentPage()
                            }
                            d.dismiss()
                            dismiss()
                        }
                    }.create().show()
            }
        }
        viewModel.dateSetObserver.observe(viewLifecycleOwner) { date -> binding.tviewsTargetDate.text = date }
        viewModel.selectPriorityObserver.observe(viewLifecycleOwner) { stringSelection ->
            run {
                binding.tviewsPriority.text = stringSelection.name
                viewModel.updatePriority(stringSelection)
            }
        }

        binding.csSubmit.setOnClickListener { viewModel.createTask(binding.etextsName.text.toString() , binding.etextsBudget.text.toString().toFloat()) }
        binding.csCancel.setOnClickListener { dismiss() }

        return binding.root
    }

}