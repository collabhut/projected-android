package rickroydaban.projects.android.projected.data;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rickroydaban.projects.android.projected.ui.home.connect.HomeConnectApi;
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksApi;

public interface UserApi {

    @POST(HomeTasksApi.ENDPOINT)
    Observable<HomeTasksApi.Response> myTasks(@Body UserRequest body);

    @POST(HomeConnectApi.ENDPOINT)
    Observable<HomeConnectApi.Response> myConnections(@Body UserRequest body);

}
