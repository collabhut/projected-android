package rickroydaban.projects.android.projected.ui.task_details

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.App
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskDetailsActivityBinding
import rickroydaban.projects.android.projected.ui.task_details.add_subtask.AddSubtaskDialog
import rickroydaban.projects.android.projected.ui.task_details.edit.EditTaskDialog
import rickroydaban.projects.android.projected.ui.task_details.create_post.TaskDetailsCreatePostDialog
import rickroydaban.projects.android.projected.ui.task_details.user_list.TaskUserListActivity
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener

class TaskDetailsActivity: AppCompatActivity() {

    companion object{
        const val INTENTARG_PROJECTID = "projectID"
        const val INTENTARG_TASKID = "taskID"
    }

    private lateinit var binding: TaskDetailsActivityBinding
    private lateinit var loader: LoaderView
    lateinit var viewModel: TaskDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TaskDetailsActivityBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(TaskDetailsViewModel::class.java)
        loader = LoaderView(binding.loader)
        setContentView(binding.root)
        //init views
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        binding.recyclerview.addOnItemTouchListener(
            RecyclerItemClickListener(
                this@TaskDetailsActivity, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        viewModel.onItemClick(position)
                    }
                })
        )
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadTaskDetails()
        }

        //observe observers
        viewModel.adapterSetupObserver.observe(this, {adapter -> binding.recyclerview.adapter = adapter})
        viewModel.loadObserver.observe(this, {showLoader -> if(showLoader) loader.show() else loader.hide()})
        viewModel.errorObserver.observe(this, { error -> error.process(this)})
        viewModel.taskDetailUpdateObserver.observe(this, {taskDetails -> updateTaskDetails(taskDetails)})
        viewModel.navigateToTaskDetailsObserver.observe(this, { task ->
            run {
                val intent = Intent(this@TaskDetailsActivity, TaskDetailsActivity::class.java)
                intent.putExtra(INTENTARG_PROJECTID, task.projectID)
                intent.putExtra(INTENTARG_TASKID, task.taskID)
                startActivity(intent)
            }
        })
        viewModel.editButtonClickObserver.observe(this, { EditTaskDialog(viewModel.projectID, viewModel.taskID, viewModel.taskDetails.componentID, viewModel.taskDetails.component, viewModel.taskDetails.name, viewModel.taskDetails.targetDate, viewModel.taskDetails.targetDateStr, viewModel.taskDetails.budget, viewModel.taskDetails.priority>0).show(supportFragmentManager, AddSubtaskDialog::class.simpleName)})
        viewModel.addSubTaskButtonClickObserver.observe(this, { AddSubtaskDialog(viewModel.projectID, viewModel.taskID, viewModel.taskDetails.name).show(supportFragmentManager, AddSubtaskDialog::class.simpleName)})
        viewModel.viewAssigneesButtonClickObserver.observe(this, { run{
            val intent = Intent(this, TaskUserListActivity::class.java)
            intent.putExtra(TaskUserListActivity.INTENTKEY_PROJECTID, viewModel.projectID)
            intent.putExtra(TaskUserListActivity.INTENTKEY_TASKID, viewModel.taskID)
            startActivity(intent)
        } })
        viewModel.postUpdateButtonClickObserver.observe(this, { TaskDetailsCreatePostDialog(viewModel.projectID, viewModel.taskID).show(supportFragmentManager, TaskDetailsCreatePostDialog::class.java.name )})
        viewModel.setTaskDetailCompletedSuccessObserver.observe(this, { AlertDialog.Builder(this).setTitle("").setMessage(R.string.success).setPositiveButton(R.string.ok) { d, _ -> d.dismiss() }.setOnDismissListener{ viewModel.loadTaskDetails()}.create().show() })

        val parentTaskString = resources.getString(R.string.parent_task)
        val subTaskString = resources.getString(R.string.subtasks)
        viewModel.init(intent.getLongExtra(INTENTARG_PROJECTID, 0), intent.getLongExtra(INTENTARG_TASKID, 0), parentTaskString, subTaskString, (application as App).customTextViewTypeFace)
        //view listeners
        binding.iviewsCompleted.setOnClickListener { viewModel.setTaskCompleted() }
//        binding.csActivities.setOnClickListener {
//            startActivity(Intent(this, TaskActivityActivity::class.java))
//        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadTaskDetails()
    }

    private fun updateTaskDetails(task: TaskDetailsApi.Task){
        if(task.isCompleted > 0){
            ViewCompat.setBackgroundTintList(
                binding.iviewsCompleted,
                ColorStateList.valueOf(resources.getColor( R.color.green))
            )
        }else{
            if(task.pending > 0){
                binding.iviewsCompleted.visibility = View.GONE
                binding.tviewsCount.visibility = View.VISIBLE
                binding.tviewsCount.text = task.pending.toString()
            }else{
                binding.iviewsCompleted.visibility = View.VISIBLE
                binding.tviewsCount.visibility = View.GONE
                ViewCompat.setBackgroundTintList(
                    binding.iviewsCompleted,
                    ColorStateList.valueOf(resources.getColor( R.color.white5))
                )
            }
        }

        binding.tviewsTitle.setTextColor(resources.getColor(R.color.black))
        val span = SpannableString(resources.getString(R.string.task_row_title, task.projectName, task.component, task.taskID.toString(), task.name))
        span.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue3)),
            0,
            span.toString().indexOf(" #" + task.taskID.toString()),
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
        span.setSpan(
            UnderlineSpan(),
            0,
            span.toString().indexOf(" #" + task.taskID.toString()),
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
        binding.tviewsTitle.text = span
    }
}