package rickroydaban.projects.android.projected.ui.home.projects

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding

class HomeProjectsFooterHolder(itemView: View, val binding: CommonsRowSpace17Binding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): HomeProjectsFooterHolder {
            val binding = CommonsRowSpace17Binding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HomeProjectsFooterHolder(binding.root, binding)
        }
    }
}