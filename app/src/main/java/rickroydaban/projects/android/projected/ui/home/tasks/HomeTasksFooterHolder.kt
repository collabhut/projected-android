package rickroydaban.projects.android.projected.ui.home.tasks

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.databinding.CommonsRowSpace17Binding

class HomeTasksFooterHolder(itemView: View, val binding: CommonsRowSpace17Binding) : RecyclerView.ViewHolder(itemView) {

    companion object{
        fun new(parent: ViewGroup): HomeTasksFooterHolder {
            val binding = CommonsRowSpace17Binding.inflate(LayoutInflater.from(parent.context), parent, false)
            return HomeTasksFooterHolder(binding.root, binding)
        }
    }
}