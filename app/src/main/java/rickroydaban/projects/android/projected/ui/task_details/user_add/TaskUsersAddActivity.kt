package rickroydaban.projects.android.projected.ui.task_details.user_add

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import rickroydaban.projects.android.projected.databinding.TaskUsersAddActivityBinding
import rickroydaban.projects.android.projected.ui.task_details.user_add.user_access.TaskUserSetUserAccessDialog
import rickroydaban.projects.android.projected.utils.LoaderView

class TaskUsersAddActivity: AppCompatActivity() {

    companion object {
        const val INTENTKEY_PROJECTID = "pid"
        const val INTENTKEY_TASKID = "tid"
    }

    private lateinit var binding: TaskUsersAddActivityBinding
    lateinit var viewModel: TaskUsersAddViewModel
    private lateinit var loader: LoaderView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = TaskUsersAddActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this).get(TaskUsersAddViewModel::class.java)
        loader = LoaderView(binding.loader)
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
//        binding.recyclerview.addOnItemTouchListener(
//            RecyclerItemClickListener(
//                activity, object : RecyclerItemClickListener.OnItemClickListener {
//                    override fun onItemClick(view: View?, position: Int) {
//                        viewModel.onItemClick(position)
//                    }
//                })
//        )

        viewModel.adapterSetupObserver.observe(this, { adapter -> binding.recyclerview.adapter = adapter })
        viewModel.loadObserver.observe(this, { showLoader -> if (showLoader) loader.show() else loader.hide() })
        viewModel.errorObserver.observe(this, { error -> error.process(this) })
//        viewModel.addUserButtonClickObserver.observe(this, { projectUserID -> viewModel.addUser(projectUserID)})
        viewModel.addUserButtonClickObserver.observe(this, { projectUser -> TaskUserSetUserAccessDialog(viewModel.projectID, viewModel.taskID, projectUser.projectUserID, "${projectUser.firstName} ${projectUser.lastName}").show(supportFragmentManager, TaskUserSetUserAccessDialog::class.simpleName)})
        viewModel.onAddUserSuccessObserver.observe(this, { viewModel.loadUsers() })
//        viewModel.navigateToTaskDetailsObserver.observe(viewLifecycleOwner, { myTask ->
//            run {
//                val intent = Intent(activity, TaskDetailsActivity::class.java)
//                intent.putExtra(TaskDetailsActivity.INTENTARG_PROJECTID, myTask.projectID)
//                intent.putExtra(TaskDetailsActivity.INTENTARG_TASKID, myTask.taskID)
//                startActivity(intent)
//            }
//        })

        viewModel.init(intent.getLongExtra(INTENTKEY_PROJECTID, 0), intent.getLongExtra(INTENTKEY_TASKID, 0))
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            viewModel.loadUsers()
        }
        binding.csClose.setOnClickListener { finish() }
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadUsers()

    }
}
