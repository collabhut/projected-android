package rickroydaban.projects.android.projected.ui.component_rename

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class ComponentRenameApi {
    companion object{
        const val ENDPOINT: String = "project/renamecomponent.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("component_id") val componentID: Long,
        @SerializedName("name") val name: String
    ): UserRequest()
}