package rickroydaban.projects.android.projected.data

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import rickroydaban.projects.android.projected.App
import rickroydaban.projects.android.projected.BuildConfig
import java.util.concurrent.TimeUnit

class ApiManager private constructor(retrofit: Retrofit) {
    var userApi: UserApi = retrofit.create(UserApi::class.java)
    var projectApi: ProjectApi = retrofit.create(ProjectApi::class.java)
    var taskApi: TaskApi = retrofit.create(TaskApi::class.java)

    companion object {
        private lateinit var instance: ApiManager

        fun init(app: App) {
            val retrofit = Retrofit.Builder()
                .baseUrl(SharedPref.get().defaultApiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(setHttpClient())
                .build()

            instance = ApiManager(retrofit)
        }

        fun get(): ApiManager {
            return instance
        }

        private fun setHttpLogger(): HttpLoggingInterceptor {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            return interceptor
        }

        private fun setHttpClient(): OkHttpClient {
            val builder: OkHttpClient.Builder = OkHttpClient().newBuilder()
            builder.readTimeout(1, TimeUnit.MINUTES)
            builder.connectTimeout(1, TimeUnit.MINUTES)
//        builder.addInterceptor(Interceptor { chain: Interceptor.Chain ->
//            val requestBuilder: OkHttpClient.Builder = chain.request().newBuilder()
//            if (headers != null) {
//                val keys = headers.keys
//                for (key in keys) {
//                    requestBuilder.addHeader(key, headers[key])
//                }
//            }
//            chain.proceed(requestBuilder.build())
//        })
            return builder.addInterceptor(setHttpLogger()).build()
        }

    }
}