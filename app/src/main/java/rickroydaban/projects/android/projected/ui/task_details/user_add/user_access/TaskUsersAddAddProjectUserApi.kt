package rickroydaban.projects.android.projected.ui.task_details.user_add.user_access

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class TaskUsersAddAddProjectUserApi {
    companion object{
        const val ENDPOINT: String = "task/addprojectusertotask.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long,
                  @SerializedName("project_user_id") val projectUserID: Long,
                  @SerializedName("can_edit") val canEdit: Int,
                  @SerializedName("can_add_subtask") val canAddSubtask: Int,
                  @SerializedName("can_see_users") val canSeeUsers: Int,
                  @SerializedName("can_see_budget") val canSeeBudget: Int,
                  @SerializedName("can_post") val canPost: Int
    ): UserRequest()
}