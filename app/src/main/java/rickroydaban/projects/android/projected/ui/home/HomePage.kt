package rickroydaban.projects.android.projected.ui.home

import androidx.fragment.app.Fragment

abstract class HomePage : Fragment() {
    abstract fun reloadPage()
}