package rickroydaban.projects.android.projected.ui.task_details.edit

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.SpannableString
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskCreateDialogBinding
import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsActivity
import rickroydaban.projects.android.projected.utils.FullWidthDialogFragment
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.SimpleTextWatcher
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class EditTaskDialog(val projectID: Long,
                     val taskID: Long,
                     val componentID: Long,
                     val componentName: String,
                     val taskName: String,
                     val targetDate:Long,
                     val targetdateStr: String,
                     val budget: Float,
                     val priority: Boolean): FullWidthDialogFragment() {
    private lateinit var binding: TaskCreateDialogBinding
    private lateinit var viewModel: EditTaskViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TaskCreateDialogBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(EditTaskViewModel::class.java)
        viewModel.init(projectID, taskID, componentID, targetDate, priority)
        binding.tviewsName.text = resources.getString(R.string.edit_task)

        binding.csProject.visibility = View.GONE
        binding.tviewsComponent.setOnClickListener { viewModel.loadComponents() }
        binding.etextsName.addTextChangedListener(SimpleTextWatcher(object: SimpleTextWatcher.Callback{
            override fun afterTextChanged(s: Editable?) {
                binding.labelName.visibility = if(s!!.isNotEmpty()) View.VISIBLE else View.GONE
                binding.etextsName.setBackgroundResource(if(s.length < EditTaskViewModel.MIN_NAME) R.drawable.bg_roundedrec_border_error else R.drawable.bg_roundedrec_border_black3)
            }
        }))
        binding.tviewsTargetDate.setOnClickListener { viewModel.datePickerDialog.show(childFragmentManager, DatePickerDialog::class.java.name) }
        binding.tviewsPriority.setOnClickListener { viewModel.prioritySelectionBottomSheet(
            resources.getString(R.string.yes),
            resources.getString(R.string.no),
            resources.getString(R.string.priority_question)
        ).show(childFragmentManager, StringSelectionBottomSheet::class.java.name) }

        binding.tviewsComponent.text = componentName
        binding.etextsName.setText(taskName)
        binding.tviewsTargetDate.text = targetdateStr
        binding.etextsBudget.setText(budget.toString())
        binding.tviewsPriority.text = resources.getString(if(priority) R.string.yes else R.string.no)

        viewModel.loadObserver.observe(this, {showLoader -> if(showLoader) loader.show() else loader.hide()})
        viewModel.errorObserver.observe(this, { error -> error.process(activity)})
        viewModel.componentListLoadSuccessObserver.observe(viewLifecycleOwner, { items -> run{ StringSelectionBottomSheet(resources.getString(R.string.select_component), items, false,  viewModel.selectComponentObserver).show(childFragmentManager, StringSelectionBottomSheet::class.java.name) }})
        viewModel.selectComponentObserver.observe(viewLifecycleOwner, { stringSelection ->
            run{
                binding.tviewsComponent.text = stringSelection.name
                viewModel.updateComponent(stringSelection)
            }
        })
        viewModel.subTaskCreateSuccessObserver.observe(viewLifecycleOwner, {
            run{
                AlertDialog.Builder(activity).setTitle("").setMessage(R.string.success).setPositiveButton(R.string.ok) { d, _ -> run{
                    if(activity is TaskDetailsActivity){
                        (activity as TaskDetailsActivity).viewModel.loadTaskDetails()
                    }

                    d.dismiss()
                    dismiss()
                } }.create().show()
            }
        })
        viewModel.dateSetObserver.observe(viewLifecycleOwner, { date -> binding.tviewsTargetDate.text = date })
        viewModel.selectPriorityObserver.observe(viewLifecycleOwner, { stringSelection ->
            run{
                binding.tviewsPriority.text = stringSelection.name
                viewModel.updatePriority(stringSelection)
            }})

        binding.csSubmit.setOnClickListener { viewModel.edit(binding.etextsName.text.toString() , binding.tviewsTargetDate.text.toString(), binding.etextsBudget.text.toString().toFloat()) }
        binding.csCancel.setOnClickListener { dismiss() }

        return binding.root
    }

}