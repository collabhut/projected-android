package rickroydaban.projects.android.projected.ui.project_details.project_components

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import rickroydaban.projects.android.projected.data.ApiError
import rickroydaban.projects.android.projected.data.ApiManager
import rickroydaban.projects.android.projected.ui.component_rename.ComponentRenameApi
import rickroydaban.projects.android.projected.utils.StringSelectionBottomSheet

class ProjectComponentsViewModel: ViewModel() {
    val adapterSetupObserver = MutableLiveData<ProjectComponentsAdapter>()
    val loadObserver = MutableLiveData<Boolean>()
    val errorObserver = MutableLiveData<ApiError>()
    val componentItemSelectedObserver = MutableLiveData<ProjectComponentsApi.Component>()
    val optionSelectedObserver = MutableLiveData<StringSelectionBottomSheet.StringSelectionString>()
    val componentDeleteSuccessObserver = MutableLiveData<Boolean>()

    var job: Job? = null
    var projectID: Long = 0
    private val header = ProjectComponentsHeader()
    private val footer = ProjectComponentsFooter()
    private var items = ArrayList<ProjectComponentsItem>()
    private var adapter = ProjectComponentsAdapter(items)

    fun init(projectID: Long){
        this@ProjectComponentsViewModel.projectID = projectID
        adapterSetupObserver.postValue(adapter)
    }

    fun loadComponents(){
        loadObserver.postValue(true)
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = ApiManager.get().projectApi.componentList(ProjectComponentsApi.Request(projectID))
            withContext(Dispatchers.Main){
                loadObserver.postValue(false)
                response.body()?.let { body ->
                    if(body.success){
                        items.clear()
                        items.add(header)
                        items.addAll(body.components)
                        items.add(footer)
                        adapter.notifyDataSetChanged()
                    }else{
                        errorObserver.postValue(body.getError())
                    }
                }
            }
        }
    }

    fun onItemClick(pos: Int, ){
        if(items[pos] is ProjectComponentsApi.Component){
            componentItemSelectedObserver.postValue(items[pos] as ProjectComponentsApi.Component)
        }
    }

    fun getOptionsBottomSheet(editStr: String, deleteStr: String): StringSelectionBottomSheet{
        var strings = ArrayList<StringSelectionBottomSheet.StringSelectionString>()
        strings.add(StringSelectionBottomSheet.StringSelectionString(1, editStr))
        strings.add(StringSelectionBottomSheet.StringSelectionString(2, deleteStr))
        return StringSelectionBottomSheet("", strings, false, optionSelectedObserver)
    }

    fun deleteComponent(componentID: Long){
        if(projectID > 0 && componentID > 0){
            loadObserver.postValue(true)
            job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
                val response = ApiManager.get().projectApi.componentDelete(ComponentDeleteApi.Request(projectID, componentID))
                withContext(Dispatchers.Main){
                    loadObserver.postValue(false)
                    response.body()?.let { body ->
                        if(body.success){
                            componentDeleteSuccessObserver.postValue(true)
                        }else{
                            errorObserver.postValue(body.getError())
                        }
                    }
                }
            }
        }else{
            errorObserver.postValue(ApiError(0, "Please fill-up required fields"))
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("TESTX","Exception handled: ${throwable.localizedMessage}")
        loadObserver.postValue(false)
        errorObserver.postValue(ApiError(0, throwable.localizedMessage))
    }

}