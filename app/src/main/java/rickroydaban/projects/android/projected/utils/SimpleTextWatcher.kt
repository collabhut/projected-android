package rickroydaban.projects.android.projected.utils

import android.text.TextWatcher
import android.text.Editable

class SimpleTextWatcher(private val callback: Callback) : TextWatcher {
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        callback.afterTextChanged(s)
    }

    interface Callback {
        fun afterTextChanged(s: Editable?)
    }
}