package rickroydaban.projects.android.projected.ui.task_details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import rickroydaban.projects.android.projected.databinding.TaskDetailsActivityDetailsActivityBinding

class TaskActivityActivity: AppCompatActivity() {
    private lateinit var binding: TaskDetailsActivityDetailsActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TaskDetailsActivityDetailsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}