package rickroydaban.projects.android.projected.ui.task_details

import android.graphics.Typeface
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.utils.CustomTypefaceSpan

class TaskDetailsAdapter(var items: List<TaskDetailsItem>,
                         private val editButtonClickObserver: MutableLiveData<Boolean>,
                         private val addSubTaskButtonClickObserver: MutableLiveData<Boolean>,
                         private val viewAssigneesButtonClickObserver: MutableLiveData<Boolean>,
                         private val postUpdateButtonClickObserver: MutableLiveData<Boolean>,
                         private val customTypefaceSpan: Typeface
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_HEADER: Int = 0
    private val ROW_ACTIONS: Int = 2
    private val ROW_TASK: Int = 3
    private val ROW_DATE: Int = 4
    private val ROW_TRAIL: Int = 5
    private val ROW_FOOTER: Int = 6
    private var canSeeUsers: Boolean = false

    override fun getItemViewType(position: Int): Int {
        return when(items[position]){
            is TaskDetailsHeader -> ROW_HEADER
            is TaskDetailsActions -> ROW_ACTIONS
            is TaskDetailsApi.Task -> ROW_TASK
            is TaskDetailsDate -> ROW_DATE
            is TaskDetailsTrail -> ROW_TRAIL
            else -> ROW_FOOTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ROW_HEADER -> TaskDetailsHeaderHolder.new(parent)
            ROW_ACTIONS -> TaskDetailsActionsHolder.new(parent, editButtonClickObserver, addSubTaskButtonClickObserver, viewAssigneesButtonClickObserver, postUpdateButtonClickObserver)
            ROW_TASK -> TaskDetailsTaskHolder.new(parent)
            ROW_DATE -> TaskDetailsDateHolder.new(parent)
            ROW_TRAIL -> TaskDetailsTrailHolder.new(parent)
            else -> TaskDetailsFooterHolder.new(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is TaskDetailsHeaderHolder -> {
                val header: TaskDetailsHeader = items[position] as TaskDetailsHeader
                holder.bind(header)
            }

            is TaskDetailsTaskHolder -> {
                val task: TaskDetailsApi.Task = items[position] as TaskDetailsApi.Task
                holder.bind(task)
            }

            is TaskDetailsActionsHolder -> {
                val taskDetailsActions: TaskDetailsActions = items[position] as TaskDetailsActions
                holder.bind(taskDetailsActions)
            }

            is TaskDetailsDateHolder -> {
                val taskDetailsDate: TaskDetailsDate = items[position] as TaskDetailsDate
                holder.bind(taskDetailsDate)
            }

            is TaskDetailsTrailHolder -> {
                val taskDetailsTrail: TaskDetailsTrail = items[position] as TaskDetailsTrail
                holder.bind(taskDetailsTrail, canSeeUsers, customTypefaceSpan)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setCanSeeUsers(canSeeUsers: Boolean){
        this@TaskDetailsAdapter.canSeeUsers = canSeeUsers
    }
}