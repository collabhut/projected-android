package rickroydaban.projects.android.projected.data;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import rickroydaban.projects.android.projected.App;

public class SharedPref {
    private static SharedPref instance;

    private final String AUTH = "auth";

    public static void init(App app){ //should only be called from the app class
        instance = new SharedPref(app);
    }

    public static final SharedPref get(){
        return instance;
    }

    private SharedPreferences pref;
    private SharedPref(){}
    private App app;

    private SharedPref(App app){
        pref = PreferenceManager.getDefaultSharedPreferences(app);
        this.app = app;
    }


//    public String getDefaultApiUrl(){ return pref.getString(API_URL, BuildConfig.API_URL); }
    public String getDefaultApiUrl(){ return "http://www.collabturf.com/projected/"; }
//    public String getDefaultApiUrl(){ return "http://192.168.43.92:8080/projected-api/"; } //NOTE 9 Hotspot
//    public String getDefaultApiUrl(){ return "http://192.168.199.215:8080/projected-api/"; } //GALAXY A7 Hotspot
//  public String getDefaultApiUrl(){ return "http://192.168.254.105:8080/projected-api/"; } //JESSEL Globe At Home Pocket Wifi
////    public String getDefaultApiUrl(){ return "http://192.168.1.16:8080/projected-api/"; }
//      public String getDefaultApiUrl(){ return "http://192.168.1.3:8080/projected-api/"; }

    public String getAuth(){ return pref.getString(AUTH, "123"); } //rickroydaban@gmail.com
//    public String getAuth(){ return "u7skDW36I1N34SiXdI6mfPlcn2y1"; } //rexjohnaban@gmail.com
}
