package rickroydaban.projects.android.projected.ui.task_details.user_list

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class TaskUserListApi {
    companion object{
        const val ENDPOINT: String = "task/users.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long,
                  @SerializedName("task_id") val taskID: Long): UserRequest()

    class Response(@SerializedName("task_users") val taskUsers: List<TaskUser>): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class TaskUser(
        @SerializedName("task_user_id") val taskUserID: Long,
        @SerializedName("fname") val firstName: String,
        @SerializedName("lname") val lastName: String,
        @SerializedName("title") val title: String,
        @SerializedName("edit") val edit: Int,
        @SerializedName("add_subtask") val addSubtask: Int,
        @SerializedName("see_users") val seeUsers: Int,
        @SerializedName("see_budget") val seeBudget: Int,
        @SerializedName("post_update") val postUpdate: Int
    ): TaskUserListItem
}