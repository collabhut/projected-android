package rickroydaban.projects.android.projected.ui.home.projects

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import rickroydaban.projects.android.projected.databinding.HomeProjectsFragmentBinding
import rickroydaban.projects.android.projected.ui.home.HomePage
import rickroydaban.projects.android.projected.ui.project_details.ProjectDetailsActivity
import rickroydaban.projects.android.projected.utils.LoaderView
import rickroydaban.projects.android.projected.utils.RecyclerItemClickListener

class HomeProjectsFragment : HomePage() {

    private lateinit var binding: HomeProjectsFragmentBinding
    private lateinit var viewModel: HomeProjectsViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeProjectsFragmentBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this).get(HomeProjectsViewModel::class.java)
        loader = LoaderView(binding.loader)
        val gridLayoutManager = GridLayoutManager(activity, 2)
        gridLayoutManager.spanSizeLookup = (object: GridLayoutManager.SpanSizeLookup(){
            override fun getSpanSize(position: Int): Int {
                return if(position == 0 || position == binding.recyclerview.adapter!!.itemCount-1) 2 else 1
            }
        })
        binding.recyclerview.layoutManager = gridLayoutManager

        binding.recyclerview.addOnItemTouchListener(
            RecyclerItemClickListener(
                activity, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {
                        viewModel.onItemClick(position)
                    }
                })
        )

        viewModel.adapterSetupObserver.observe(viewLifecycleOwner) { adapter -> binding.recyclerview.adapter = adapter }
        viewModel.loadObserver.observe(viewLifecycleOwner) { showLoader -> if (showLoader) loader.show() else loader.hide() }
        viewModel.errorObserver.observe(viewLifecycleOwner) { error -> error.process(context) }
        viewModel.navigateToProjectDetailsObserver.observe(viewLifecycleOwner) { myProject ->
            run {
                val intent = Intent(activity, ProjectDetailsActivity::class.java)
                intent.putExtra(ProjectDetailsActivity.INTENTKEY_PROJECTID, myProject.projectID)
                intent.putExtra(ProjectDetailsActivity.INTENTKEY_NAME, myProject.name)
                startActivity(intent)
            }
        }

        viewModel.init()
        binding.swiperefreshlayout.setOnRefreshListener {
            binding.swiperefreshlayout.isRefreshing = false
            reloadPage()
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        reloadPage()
    }

    override fun reloadPage() {
        viewModel.loadMyProjects()
    }
}