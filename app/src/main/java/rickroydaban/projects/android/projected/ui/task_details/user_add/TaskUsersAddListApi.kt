package rickroydaban.projects.android.projected.ui.task_details.user_add

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.ApiResponse
import rickroydaban.projects.android.projected.data.UserRequest

class TaskUsersAddListApi {
    companion object{
        const val ENDPOINT: String = "task/projectuserstoadd.php"
    }

    class Request(@SerializedName("project_id") val projectID: Long, @SerializedName("task_id") val taskID: Long): UserRequest()

    class Response(@SerializedName("project_users") val projectUsers: List<ProjectUser>): ApiResponse(false, "Please contact developer: invalid params @${this.javaClass.name} ", 1)

    class ProjectUser(
        @SerializedName("project_user_id") val projectUserID: Long,
        @SerializedName("fname") val firstName: String,
        @SerializedName("lname") val lastName: String,
        @SerializedName("title") val title: String
    ): TaskUsersAddItem
}