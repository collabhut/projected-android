package rickroydaban.projects.android.projected.ui.project_details.project_users

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.ProjectUserListUserRowBinding

class ProjectUsersUserHolder(itemView: View, val binding: ProjectUserListUserRowBinding, val optionsButtonClickObserver: MutableLiveData<ProjectUsersApi.ProjectUser>) : RecyclerView.ViewHolder(itemView) {

    companion object{

        fun new(parent: ViewGroup, optionsButtonClickObserver: MutableLiveData<ProjectUsersApi.ProjectUser>): ProjectUsersUserHolder {
            val binding = ProjectUserListUserRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ProjectUsersUserHolder(binding.root, binding, optionsButtonClickObserver)
        }
    }

    init {
        binding.iviewsOptions.setOnClickListener { optionsButtonClickObserver.postValue(itemView.tag as ProjectUsersApi.ProjectUser) }
    }

    fun bind(projectUser: ProjectUsersApi.ProjectUser){
        itemView.tag = projectUser
        binding.tviewsTitle.text = "${projectUser.firstName} ${projectUser.lastName}"
        binding.tviewsSubtitle.text = projectUser.title
        ViewCompat.setBackgroundTintList(binding.iviewsEdit, ColorStateList.valueOf(itemView.resources.getColor(if(projectUser.edit > 0) R.color.green else R.color.white5)))
        ViewCompat.setBackgroundTintList(binding.iviewsAddTask, ColorStateList.valueOf(itemView.resources.getColor(if(projectUser.addTask > 0) R.color.green else R.color.white5)))
    }
}