package rickroydaban.projects.android.projected.ui.task_details.user_list.actions

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskUserListActionBottomsheetBinding
import rickroydaban.projects.android.projected.ui.task_details.user_list.TaskUserListActivity
import rickroydaban.projects.android.projected.ui.task_details.user_list.edit_user_access.TaskUserEditAccessDialog
import rickroydaban.projects.android.projected.ui.task_details.user_list.edit_user_access.TaskUserEditAccessViewModel
import rickroydaban.projects.android.projected.utils.LoaderView

class TaskUserListActionBottomSheet(val projectID: Long, val taskID: Long, val taskUserID: Long, val taskUserName: String,
                                    val canEdit: Boolean, val canAddSubtask: Boolean, val canSeeUsers: Boolean, val canSeeBudget: Boolean, val canPost: Boolean): BottomSheetDialogFragment() {
    private lateinit var binding: TaskUserListActionBottomsheetBinding
    private lateinit var viewModel: TaskUserListActionViewModel
    private lateinit var loader: LoaderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = TaskUserListActionBottomsheetBinding.inflate(layoutInflater)
        loader = LoaderView(binding.loader)
        viewModel = ViewModelProvider(this).get(TaskUserListActionViewModel::class.java)
        viewModel.init(projectID, taskID, taskUserID)

        val span = SpannableString(resources.getString(R.string.select_action_for, taskUserName))
        span.setSpan(ForegroundColorSpan(resources.getColor(R.color.blue3)), span.toString().indexOf(taskUserName), span.toString().indexOf(taskUserName)+taskUserName.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        binding.tviewsTitle.text = span

        binding.csEdit.setOnClickListener { TaskUserEditAccessDialog(projectID, taskID, taskUserID, taskUserName, canEdit, canAddSubtask, canSeeUsers, canSeeBudget, canPost, this).show(childFragmentManager, TaskUserEditAccessDialog::class.java.name) }
        binding.csRemove.setOnClickListener {
            AlertDialog.Builder(binding.csRemove.context).setTitle("").setMessage(resources.getString(R.string.are_you_sure_to_remove_user_from_this_task, taskUserName))
                .setPositiveButton(R.string.yes){ d,_ ->
                    run {
                        viewModel.removeUser()
                        d.dismiss()
                    }
                }.setNegativeButton(R.string.no){ d, _ -> d.dismiss()}.create().show()
        }
        binding.csClose.setOnClickListener { dismiss() }

        viewModel.loadObserver.observe(viewLifecycleOwner, {showLoader -> if(showLoader) loader.show() else loader.hide()})
        viewModel.errorObserver.observe(viewLifecycleOwner, { error -> error.process(activity)})
        viewModel.onRemoveUserSuccessObserver.observe(viewLifecycleOwner, { android.app.AlertDialog.Builder(activity).setTitle("").setMessage(R.string.success).setPositiveButton(R.string.ok) { d, _ -> d.dismiss() }.setOnDismissListener {
            if(activity is TaskUserListActivity){
                (activity as TaskUserListActivity).viewModel.loadUsers()
            }
            dismiss()
        }.create().show()})

        return binding.root
    }
}