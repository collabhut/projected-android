package rickroydaban.projects.android.projected.utils

import android.graphics.PorterDuff
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.LoaderBinding

class LoaderView(var binding: LoaderBinding) {

    fun init(){
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            binding.progressbar.progressDrawable.setColorFilter(binding.root.resources.getColor(R.color.blue3), PorterDuff.Mode.SRC_IN)
        } else {
            binding.progressbar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(binding.root.context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN)
        }
    }

    fun show() {
        binding.root.visibility = View.VISIBLE
    }

    fun hide() {
        binding.root.visibility = View.GONE
    }
}