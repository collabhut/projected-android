package rickroydaban.projects.android.projected.ui.component_create

import com.google.gson.annotations.SerializedName
import rickroydaban.projects.android.projected.data.UserRequest

class ComponentCreateApi {
    companion object{
        const val ENDPOINT: String = "project/createcomponent.php"
    }

    class Request(
        @SerializedName("project_id") val projectID: Long,
        @SerializedName("name") val name: String
    ): UserRequest()
}