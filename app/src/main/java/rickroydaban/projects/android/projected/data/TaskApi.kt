package rickroydaban.projects.android.projected.data

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import rickroydaban.projects.android.projected.ui.home.tasks.HomeTasksApi
import rickroydaban.projects.android.projected.ui.task_details.SetTaskCompletedApi
import rickroydaban.projects.android.projected.ui.task_details.add_subtask.AddSubtaskApi
import rickroydaban.projects.android.projected.ui.task_details.TaskDetailsApi
import rickroydaban.projects.android.projected.ui.task_details.create_post.TaskDetailsCreatePostApi
import rickroydaban.projects.android.projected.ui.task_details.edit.EditTaskApi
import rickroydaban.projects.android.projected.ui.task_details.user_add.TaskUsersAddListApi
import rickroydaban.projects.android.projected.ui.task_details.user_add.user_access.TaskUsersAddAddProjectUserApi
import rickroydaban.projects.android.projected.ui.task_details.user_list.TaskUserListApi
import rickroydaban.projects.android.projected.ui.task_details.user_list.actions.TaskUserRemoveApi
import rickroydaban.projects.android.projected.ui.task_details.user_list.edit_user_access.TaskUserEditAccessApi

interface TaskApi {
    @POST(HomeTasksApi.ENDPOINT)
    suspend fun myTasks(@Body body: UserRequest): Response<HomeTasksApi.Response?>

    @POST(TaskDetailsApi.ENDPOINT)
    suspend fun details(@Body body: TaskDetailsApi.Request): Response<TaskDetailsApi.Response?>

    @POST(EditTaskApi.ENDPOINT)
    suspend fun edit(@Body body: EditTaskApi.Request): Response<ApiResponse>

    @POST(AddSubtaskApi.ENDPOINT)
    suspend fun createSubTask(@Body body: AddSubtaskApi.Request): Response<ApiResponse>

    @POST(TaskUserListApi.ENDPOINT)
    suspend fun usersToAdd(@Body body: TaskUserListApi.Request): Response<TaskUserListApi.Response>

    @POST(SetTaskCompletedApi.ENDPOINT)
    suspend fun setTaskCompleted(@Body body: SetTaskCompletedApi.Request): Response<ApiResponse>

    @POST(TaskUsersAddListApi.ENDPOINT)
    suspend fun usersToAdd(@Body body: TaskUsersAddListApi.Request): Response<TaskUsersAddListApi.Response>

    @POST(TaskUsersAddAddProjectUserApi.ENDPOINT)
    suspend fun addProjectUser(@Body body: TaskUsersAddAddProjectUserApi.Request): Response<ApiResponse>

    @POST(TaskUserEditAccessApi.ENDPOINT)
    suspend fun editUserAccess(@Body body: TaskUserEditAccessApi.Request): Response<ApiResponse>

    @POST(TaskUserRemoveApi.ENDPOINT)
    suspend fun removeUser(@Body body: TaskUserRemoveApi.Request): Response<ApiResponse>

    @POST(TaskDetailsCreatePostApi.ENDPOINT)
    suspend fun createPost(@Body body: TaskDetailsCreatePostApi.Request): Response<ApiResponse>
}