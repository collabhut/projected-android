package rickroydaban.projects.android.projected.ui.project_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProjectDetailsViewModel: ViewModel(){
    var projectID: Long = 0

    fun init(projectID: Long){
        this@ProjectDetailsViewModel.projectID = projectID
    }
}