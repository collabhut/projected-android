package rickroydaban.projects.android.projected.ui.task_details.user_list

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import rickroydaban.projects.android.projected.R
import rickroydaban.projects.android.projected.databinding.TaskUserListUserRowBinding

class TaskUserListUserHolder(itemView: View, val binding: TaskUserListUserRowBinding, val optionsButtonClickObserver: MutableLiveData<TaskUserListApi.TaskUser>) : RecyclerView.ViewHolder(itemView) {

    companion object{

        fun new(parent: ViewGroup, optionsButtonClickObserver: MutableLiveData<TaskUserListApi.TaskUser>): TaskUserListUserHolder {
            val binding = TaskUserListUserRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TaskUserListUserHolder(binding.root, binding, optionsButtonClickObserver)
        }
    }

    init {
        binding.iviewsOptions.setOnClickListener { optionsButtonClickObserver.postValue(itemView.tag as TaskUserListApi.TaskUser) }
    }

    fun bind(taskUser: TaskUserListApi.TaskUser){
        itemView.tag = taskUser
        binding.tviewsTitle.text = "${taskUser.firstName} ${taskUser.lastName}"
        binding.tviewsSubtitle.text = taskUser.title
        ViewCompat.setBackgroundTintList(binding.iviewsEdit, ColorStateList.valueOf(itemView.resources.getColor(if(taskUser.edit > 0) R.color.green else R.color.white5)))
        ViewCompat.setBackgroundTintList(binding.iviewsAddSubtask, ColorStateList.valueOf(itemView.resources.getColor(if(taskUser.addSubtask > 0) R.color.green else R.color.white5)))
        ViewCompat.setBackgroundTintList(binding.iviewsSeeUsers, ColorStateList.valueOf(itemView.resources.getColor(if(taskUser.seeUsers > 0) R.color.green else R.color.white5)))
        ViewCompat.setBackgroundTintList(binding.iviewsSeeBudget, ColorStateList.valueOf(itemView.resources.getColor(if(taskUser.seeBudget > 0) R.color.green else R.color.white5)))
        ViewCompat.setBackgroundTintList(binding.iviewsPostUpdate, ColorStateList.valueOf(itemView.resources.getColor(if(taskUser.postUpdate > 0) R.color.green else R.color.white5)))
    }
}