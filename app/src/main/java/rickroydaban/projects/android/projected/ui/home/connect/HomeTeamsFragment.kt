package rickroydaban.projects.android.projected.ui.home.connect

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import rickroydaban.projects.android.projected.databinding.HomeTeamsFragmentBinding
import rickroydaban.projects.android.projected.ui.home.HomePage

class HomeTeamsFragment() : HomePage() {
    private lateinit var binding: HomeTeamsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeTeamsFragmentBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun reloadPage() {

    }
}